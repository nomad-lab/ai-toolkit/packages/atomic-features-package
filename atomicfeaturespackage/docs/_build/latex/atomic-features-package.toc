\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Description}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Getting Started}{2}{chapter.2}
\contentsline {chapter}{\numberline {3}Importing this package modules in Jupyter notebook}{3}{chapter.3}
\contentsline {chapter}{\numberline {4}atomic\sphinxhyphen {}features\sphinxhyphen {}package package}{5}{chapter.4}
\contentsline {section}{\numberline {4.1}Subpackages}{5}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}atomicfeaturespackage.atomicproperties package}{5}{subsection.4.1.1}
\contentsline {subsubsection}{atomicfeaturespackage.atomicproperties.atomic\_properties\_dft module}{5}{subsubsection*.3}
\contentsline {subsubsection}{atomicfeaturespackage.atomicproperties.atomic\_properties\_lda2015 module}{6}{subsubsection*.9}
\contentsline {subsubsection}{atomicfeaturespackage.atomicproperties.atomic\_properties\_matminer module}{6}{subsubsection*.12}
\contentsline {subsubsection}{atomicfeaturespackage.atomicproperties.atomic\_properties\_pymat module}{6}{subsubsection*.19}
\contentsline {subsubsection}{atomicfeaturespackage.atomicproperties.periodictable module}{7}{subsubsection*.23}
\contentsline {subsection}{\numberline {4.1.2}atomicfeaturespackage.metainfo package}{9}{subsection.4.1.2}
\contentsline {subsubsection}{atomicfeaturespackage.metainfo.metainfo module}{9}{subsubsection*.50}
\contentsline {chapter}{\numberline {5}LICENSE}{22}{chapter.5}
\contentsline {chapter}{\numberline {6}CONTRIBUTING}{23}{chapter.6}
\contentsline {chapter}{\numberline {7}Indices and tables}{24}{chapter.7}
\contentsline {chapter}{Python Module Index}{25}{section*.263}
