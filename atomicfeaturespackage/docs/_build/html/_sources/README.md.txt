# Description

Unified package that contains atomic-features from various sources like PYMATGEN,MAGPIE,LDA2015,WEBELEMENTS amd FHI_dft can be imported into Jupyter-notebook for performing various machine learning studies

**-------------------------------------------------------------------------------------------------------------------------------------------------------**
# Getting Started

To install this package download it as tar.gz archive. Then use the following command

```pip install ./<name-of-tar-package>.tar.gz```

**-------------------------------------------------------------------------------------------------------------------------------------------------------**
# Importing this package modules in Jupyter notebook

```from atomicfeaturespackage.atomicproperties import periodictable```

This module is a visualization tool that allows us to visualize atomic properites of all elements accross periodic table as a heatmap. Currently, this tool is able to visualize atomic properties acessible from atomic_properties_dft and atomic_properties_lda2015 module. 

Below is an example line of code you need to run to visualize data calculated via the HSE06 functional and spinless settings.

```periodictable.heatmap(Spin = 'False', method = 'hse06')```

In method arg in heatmap function one can specify what functional one wants visualize atomic properties for. 
Currently data evaluated are available from following functionals 'HSE06', 'REVPBE', 'PW-LDA', 'PBE' , 'PBESOL', 'PBE0', 'LDA2015' .<br/>
Spin Setting can be set to 'TRUE' or 'FALSE'.

After running the above line of code one gets a plot of complete periodic table of elements. From the dropdown menu, one can select which property one is interested to check and the table is updated automatically to show the corresponding heatmap.

**-------------------------------------------------------------------------------------------------------------------------------------------------------**

```from atomicfeaturespackage.atomicproperties import atomic_properties_dft as dft```

This module contains several atomic features accessible from FHI-AIMS dft calculations

To access atomic properties calculated from different DFT functionals and spin setting one first need to instantiate Specific functional and spin setting one is interested to access using the method function of this module. This can be done as follows :

```dft.method(method = 'pbe', Spin = 'False')```

Currently data evaluated are available from following functionals 'HSE06', 'REVPBE', 'PW-LDA', 'PBE' , 'PBESOL', 'PBE0'.

Spin Setting can be set to 'TRUE' or 'FALSE'.

One can now acess individually different properties using element symbol as following

```
    dft.C.atomic_ea
    dft.C.atomic_r_p
    dft.C.atomic_ip_by_half_charged_homo
```

To get a list of properties accessible one can just press Tab after typing in `dft.C.`

Use symbol method from atomic_properties_dft module to acess property of interest of elements at once say for example we have a python list ls = ['Sc','Ti', 'V', 'Cr'] and we wish to get atomic number of these elements we can do the following

```
    atomic_number = []
    for i in ls:
        atomic_number.append(dft.symbol(i).atomic_number)

```
**-------------------------------------------------------------------------------------------------------------------------------------------------------**


```from atomicfeaturespackage.atomicproperties import atomic_properties_pymat as pymat```

This module contains several atomic features accessible from pymatgen

One can now acess individually different properties using element symbol as following

```
    pymat.C.atomic_ea
    pymat.C.atomic_r_p
    pymat.C.atomic_ip_by_half_charged_homo 
```
To get a list of properties accessible one can just press Tab after typing in `pymat.C.`

Use symbol method from atomic_properties_pymat module to acess property of interest of elements at once in similar manner as described for atomic_properties_dft module above.

**-------------------------------------------------------------------------------------------------------------------------------------------------------**

```from atomicfeaturespackage.atomicproperties import atomic_properties_lda2015 as lda```

This module contains several atomic features accessible from lda2015 paper

One can acess individually different properties using element symbol as following

```
    lda.C.atomic_ea
    lda.C.atomic_r_p
```
To get a list of properties accessible one can just press Tab after typing in `lda.C.`

Use symbol method from atomic_properties_lda2015 module to acess property of interest of elements at once in similar manner as described for atomic_properties_dft module above.

**-------------------------------------------------------------------------------------------------------------------------------------------------------**

```from atomicfeaturespackage.atomicproperties import atomic_properties_webele as webele```

This module contains several atomic features accessible from Webelements source

One can acess individually different properties using element symbol as following

```
    webele.C.atomic_radius_calculated
    webele.C.critical_temperature
```
To get a list of properties accessible one can just press Tab after typing in `webele.C.`

Use symbol method from atomic_properties_webele module to acess property of interest of elements at once in similar manner as described for atomic_properties_dft module above.
