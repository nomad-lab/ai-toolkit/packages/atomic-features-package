.. atomic-features-package documentation master file, created by
   sphinx-quickstart on Wed Apr  7 09:39:02 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to atomic-features-package's documentation!
===================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   ../README.md
   atomicfeaturespackage.rst
   ../LICENSE.md
   ../CONTRIBUTING.md
   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
