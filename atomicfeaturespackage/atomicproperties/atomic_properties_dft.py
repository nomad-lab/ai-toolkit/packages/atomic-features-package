#!/usr/bin/env python
# coding: utf-8

'''
Module : atomic_properties_dft

This module with help of  atomic_properties_fhi class from metainfo module instantiates objects with atomic element symbol as identifier to access atomic features available 
'''

from atomicfeaturespackage.metainfo.metainfo import atomic_properties_fhi,metadata
import pandas as pd
import os



def f(df1,objs):
    '''
    This method instantiates objects of class atomic_properties_fhi when spin arg is set to "FALSE".
    '''    
    
    for i in df1['atomic_element_symbol']:
        fhi = atomic_properties_fhi()
        prop = fhi.m_create(metadata)
        objs.append(fhi)
    
    count = 0
    for obj in objs:
        obj.section_fhi_metadata.source = 'FHIaims'
        obj.section_fhi_metadata.atomic_spin_setting = 'FALSE'
        obj.atomic_number = int(df1.iloc[count,df1.columns.get_loc("atomic_number")])
        obj.atomic_element_symbol = str(df1.iloc[count,df1.columns.get_loc("atomic_element_symbol")])
        obj.section_fhi_metadata.atomic_method = str(df1.iloc[count,df1.columns.get_loc("atomic_method")])
        obj.section_fhi_metadata.atomic_basis_set = str(df1.iloc[count,df1.columns.get_loc("atomic_basis_set")])
        obj.atomic_hfomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hfomo")])
        obj.atomic_hpomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hpomo")])
        obj.atomic_lfumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lfumo")])
        obj.atomic_lpumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lpumo")])
        obj.atomic_ea = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_energy_difference")])
        obj.atomic_ip = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_energy_difference")])
        obj.atomic_ea_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_half_charged_homo")])
        obj.atomic_ip_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_half_charged_homo")])
        obj.atomic_r_s_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_s_neg_1.0")])]
        obj.atomic_r_p_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_p_neg_1.0")])]
        obj.atomic_r_d__neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_d_neg_1.0")])]
        obj.atomic_r_val_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_val_neg_1.0")])]
        obj.atomic_r_s_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_s_neg_0.5")])]
        obj.atomic_r_p_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_p_neg_0.5")])]
        obj.atomic_r_d_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_d_neg_0.5")])]
        obj.atomic_r_val_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_val_neg_0.5")])]
        obj.atomic_r_s = [float(df1.iloc[count,df1.columns.get_loc("r_s_0.0")])]
        obj.atomic_r_p = [float(df1.iloc[count,df1.columns.get_loc("r_p_0.0")])]
        obj.atomic_r_d = [float(df1.iloc[count,df1.columns.get_loc("r_d_0.0")])]
        obj.atomic_r_val = [float(df1.iloc[count,df1.columns.get_loc("r_val_0.0")])]
        obj.atomic_r_s_05 = [float(df1.iloc[count,df1.columns.get_loc("r_s_0.5")])]
        obj.atomic_r_p_05 = [float(df1.iloc[count,df1.columns.get_loc("r_p_0.5")])]
        obj.atomic_r_d_05 = [float(df1.iloc[count,df1.columns.get_loc("r_d_0.5")])]
        obj.atomic_r_val_05 = [float(df1.iloc[count,df1.columns.get_loc("r_val_0.5")])]
        obj.atomic_r_s_1 = [float(df1.iloc[count,df1.columns.get_loc("r_s_1.0")])]
        obj.atomic_r_p_1 = [float(df1.iloc[count,df1.columns.get_loc("r_p_1.0")])]
        obj.atomic_r_d_1 = [float(df1.iloc[count,df1.columns.get_loc("r_d_1.0")])]
        obj.atomic_r_val_1 =[float(df1.iloc[count,df1.columns.get_loc("r_val_1.0")])] 
        count+=1
        
    atomic_element_symbol = df1['atomic_element_symbol'].values
    for count, symbol in enumerate(atomic_element_symbol):
        globals()[symbol] = objs[count]
                
        
def t(df1,objs):
    '''
    This method instantiates objects of class atomic_properties_fhi when spin arg is set to "TRUE".
    '''
    for i in df1['atomic_element_symbol']:
        fhi = atomic_properties_fhi()
        prop = fhi.m_create(metadata)
        objs.append(fhi)
        
    count = 0
    for obj in objs:
        obj.section_fhi_metadata.source = 'FHIaims'
        obj.section_fhi_metadata.atomic_spin_setting = 'TRUE'
        obj.atomic_number = int(df1.iloc[count,df1.columns.get_loc("atomic_number")])
        obj.atomic_element_symbol = str(df1.iloc[count,df1.columns.get_loc("atomic_element_symbol")])
        obj.section_fhi_metadata.atomic_method = str(df1.iloc[count,df1.columns.get_loc("atomic_method")])
        obj.section_fhi_metadata.atomic_basis_set = str(df1.iloc[count,df1.columns.get_loc("atomic_basis_set")])
        obj.atomic_hfomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hfomo")])
        obj.atomic_hpomo = float(df1.iloc[count,df1.columns.get_loc("atomic_hpomo")])
        obj.atomic_lfumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lfumo")])
        obj.atomic_lpumo = float(df1.iloc[count,df1.columns.get_loc("atomic_lpumo")])
        obj.atomic_ea = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_energy_difference")])
        obj.atomic_ip = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_energy_difference")])
        obj.atomic_ea_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ea_by_half_charged_homo")])
        obj.atomic_ip_by_half_charged_homo = float(df1.iloc[count,df1.columns.get_loc("atomic_ip_by_half_charged_homo")])
        obj.atomic_r_s_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_neg_1.0")])]
        obj.atomic_r_p_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_neg_1.0")])]
        obj.atomic_r_d__neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_dn_d_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_neg_1.0")])]
        obj.atomic_r_val_neg_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_neg_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_neg_1.0")])]
        obj.atomic_r_s_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_neg_0.5")])]
        obj.atomic_r_p_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_neg_0.5")])]
        obj.atomic_r_d_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_neg_0.5")])]
        obj.atomic_r_val_neg_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_neg_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_neg_0.5")])]
        obj.atomic_r_s = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_0.0")])]
        obj.atomic_r_p = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_0.0")])]
        obj.atomic_r_d = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_0.0")])]
        obj.atomic_r_val = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_0.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_0.0")])]
        obj.atomic_r_s_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_0.5")])]
        obj.atomic_r_p_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_0.5")])]
        obj.atomic_r_d_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_0.5")])]
        obj.atomic_r_val_05 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_0.5")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_0.5")])]
        obj.atomic_r_s_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_s_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_s_1.0")])]
        obj.atomic_r_p_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_p_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_p_1.0")])]
        obj.atomic_r_d_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_d_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_d_1.0")])]
        obj.atomic_r_val_1 = [float(df1.iloc[count,df1.columns.get_loc("r_up_val_1.0")]),float(df1.iloc[count,df1.columns.get_loc("r_dn_val_1.0")])]
        count+=1
        
    atomic_element_symbol = df1['atomic_element_symbol'].values
    for count, symbol in enumerate(atomic_element_symbol):
        globals()[symbol] = objs[count]


# 
# # This function one has to call with it params as per ones interest and method  

# In[3]:
path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))

def method(Spin = '', method = ''):
    '''
After importing atomic_properties_dft module one must call this method function to instantiate element objects. One must supply Spin and method   appropirate keywords agruments as per one's interest. 


Spin arg value could be :

* "True"

* "False"

method arg value could be :

* "hse06"

* "pbe"

* "pbe0"

* "pbesol"

* "pw-lda"

* "revpbe"

As per different combinations specific datafiles will be read and element features could be accessed
    '''

    if Spin.lower() == 'false' and method.lower() == 'hse06':
        path_new = os.path.join(path, "data","dft", "no_spin_hse06_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pbe':
        path_new = os.path.join(path, "data","dft", "no_spin_pbe_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pbe0':
        path_new = os.path.join(path, "data","dft", "no_spin_pbe0_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pbesol':
        path_new = os.path.join(path, "data","dft", "no_spin_pbesol_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='pw-lda':
        path_new = os.path.join(path, "data","dft", "no_spin_pw-lda_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'false' and method.lower() =='revpbe':
        path_new = os.path.join(path, "data","dft", "no_spin_revpbe_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        f(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='hse06':
        path_new = os.path.join(path, "data","dft", "spin_hse06_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pbe':
        path_new = os.path.join(path, "data","dft", "spin_pbe_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pbe0':
        path_new = os.path.join(path, "data","dft", "spin_pbe0_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pbesol':
        path_new = os.path.join(path, "data","dft", "spin_pbesol_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='pw-lda':
        path_new = os.path.join(path, "data","dft", "spin_pw-lda_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        t(df1,objs)
    elif Spin.lower() == 'true' and method.lower() =='revpbe':
        path_new = os.path.join(path, "data","dft", "spin_revpbe_really_tight.csv")
        df1 = pd.read_csv(path_new)
        objs = []
        t(df1,objs)
    else:
        raise Exception("Please check the input parameters or Data for specified functional and spin setting is not available")
       
    
def definition(abc):
    '''
    This function can be used to access quantity definitions for each element features accessible
    '''
    if hasattr(atomic_properties_fhi,abc):
        return print(getattr(atomic_properties_fhi,abc).__doc__)
    elif hasattr(metadata,abc):
        return print(getattr(metadata,abc).__doc__)
    else:
        return print('Check the input feature name. Either it is wrong or requested feature does not exist from dft source')    

def symbol(abc):
    '''
This function utilty is, it can be called to access element properties based on its symbol in periodic table. Can be usefull if want to acess particular property of multiple elements at once
    '''
    return globals()[abc]

# This method_list object could be called after importing this module so one can easily see all the quantities accessible.
method_list = [method for method in dir(atomic_properties_fhi) if (method.startswith('m_') or method.startswith('_') or method.startswith('__') or method.startswith('val') or method.startswith('section_') or method.startswith('get'))  is False]


