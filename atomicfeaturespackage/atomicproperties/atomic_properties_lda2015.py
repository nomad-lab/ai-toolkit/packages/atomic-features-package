#!/usr/bin/env python
# coding: utf-8

'''
Module : atomic_properties_lda2015

This module with help of  atomic_properties_lda2015 class from metainfo module instantiates objects with atomic element symbol as identifier to access atomic features available 
'''


from atomicfeaturespackage.metainfo.metainfo import atomic_properties_lda2015,metadata
import pandas as pd
import os


path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","lda2015", "Atomic_features.csv")
df1 = pd.read_csv(path_new)

# This method_list object could be called after importing this module so one can easily see all the quantities accessible.
method_list = [method for method in dir(atomic_properties_lda2015) if (method.startswith('m_') or method.startswith('_') or method.startswith('__') or method.startswith('val') or method.startswith('section_') or method.startswith('get'))  is False]


objs = []



for i in df1['atomic_element_symbol']:
        fhi = atomic_properties_lda2015()
        prop = fhi.m_create(metadata)
        objs.append(fhi)



count = 0

for obj in objs:
    obj.section_lda2015_metadata.source = 'FHIaims'
    obj.section_lda2015_metadata.atomic_spin_setting = 'None'
    obj.atomic_number = int(df1.iloc[count,df1.columns.get_loc("atomic_number")])
    obj.atomic_element_symbol = str(df1.iloc[count,df1.columns.get_loc("atomic_element_symbol")])
    obj.atomic_period = int(df1.iloc[count,df1.columns.get_loc("period")])
    obj.section_lda2015_metadata.atomic_method = 'LDA'
    obj.section_lda2015_metadata.atomic_basis_set = 'NONE'
    obj.atomic_homo = float(df1.iloc[count,df1.columns.get_loc("E_HOMO")])
    obj.atomic_lumo = float(df1.iloc[count,df1.columns.get_loc("E_LUMO")])
    obj.atomic_ea = float(df1.iloc[count,df1.columns.get_loc("EA")])
    obj.atomic_ip = float(df1.iloc[count,df1.columns.get_loc("IP")])
    obj.atomic_r_s = float(df1.iloc[count,df1.columns.get_loc("r_s")])
    obj.atomic_r_p = float(df1.iloc[count,df1.columns.get_loc("r_p")])
    obj.atomic_r_d = float(df1.iloc[count,df1.columns.get_loc("r_d")])
    count+=1

atomic_element_symbol = df1['atomic_element_symbol'].values
for count, symbol in enumerate(atomic_element_symbol):
    globals()[symbol] = objs[count]

        
def definition(abc):
    '''
    This function can be used to access quantity definitions for each element features accessible
    '''
    if hasattr(atomic_properties_lda2015,abc):
        return print(getattr(atomic_properties_lda2015,abc).__doc__)
    elif hasattr(metadata,abc):
        return print(getattr(metadata,abc).__doc__)
    else:
        return print('Check the input feature name. Either it is wrong or requested feature does not exist from lda2015 source') 

def symbol(abc):
    '''
This function utilty is, it can be called to access element properties based on its symbol in periodic table. Can be usefull if one wants to acess particular property of multiple elements at once
    '''
    return globals()[abc]

