#!/usr/bin/env python
# coding: utf-8

'''
Module : atomic_properties_matminer

This module with help of  atomic_properties_matminer class from metainfo module instantiates objects with atomic element symbol as identifier to access atomic features available 
'''

from atomicfeaturespackage.metainfo.metainfo import atomic_properties_matminer,metadata
import pandas as pd
import numpy as np
from mendeleev import element
import os


# This method_list object could be called after importing this module so one can easily see all the quantities accessible.
method_list = [method for method in dir(atomic_properties_matminer) if (method.startswith('m_') or method.startswith('_') or method.startswith('__') or method.startswith('val') or method.startswith('section_') or method.startswith('get'))  is False]



ls = list(range(0,112))
df = pd.DataFrame(index = ls,columns = method_list)



path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))


def ionizationen():
    ie = pd.read_table(os.path.join(path, "data","matminer", "IonizationEnergies.table"),header=None,sep = '\t')
    ie['combined']= ie.values.tolist()
    return ie['combined']

def oxistates():
    oxstates = pd.read_table(os.path.join(path, "data","matminer", "OxidationStates.table"),header=None,skip_blank_lines=False)
    count = 0
    for i in oxstates.iterrows():
        ls = []
        try: 
            for i in oxstates.iloc[count,0].split():
                ls.append(float(i))
                oxstates.iloc[count,0] = ls
        except TypeError and AttributeError:
            pass
        count+=1
    return oxstates

def atomicsymbol():
    count = 0
    for i in df['atomic_number']:
        df.iloc[count,5] = element(i).symbol
        count+=1
        
#Read the magpie data files and clean and arrange systematically in a pandas dataframe.
    
df['atomic_ICSD_vol'] = pd.read_table(os.path.join(path, "data","matminer", "ICSDVolume.table"),header=None)
df['atomic_cpmass'] = pd.read_table(os.path.join(path, "data","matminer", "HeatCapacityMass.table"),header=None)
df['atomic_cpmolar'] = pd.read_table(os.path.join(path, "data","matminer", "HeatCapacityMolar.table"),header=None)
df['atomic_density'] = pd.read_table(os.path.join(path, "data","matminer", "Density.table"),header=None)
df['atomic_ea'] = pd.read_table(os.path.join(path, "data","matminer", "ElectronAffinity.table"),header=None)
df['atomic_number'] = pd.read_table(os.path.join(path, "data","matminer", "Number.table"),header=None)
atomicsymbol()
df['atomic_en'] = pd.read_table(os.path.join(path, "data","matminer", "Electronegativity.table"),header=None)
df['atomic_en_allen'] =pd.read_table(os.path.join(path, "data","matminer", "AllenElectronegativity.table"),header=None)
df['atomic_hfu'] = pd.read_table(os.path.join(path, "data","matminer", "HeatFusion.table"),header=None)
df['atomic_hhip'] = pd.read_table(os.path.join(path, "data","matminer", "HHIp.table"),header=None)
df['atomic_hhir'] = pd.read_table(os.path.join(path, "data","matminer", "HHIr.table"),header=None)
df['atomic_hvap'] = pd.read_table(os.path.join(path, "data","matminer", "HeatVaporization.table"),header=None)
df['atomic_ie_1'] = pd.read_table(os.path.join(path, "data","matminer", "FirstIonizationEnergy.table"),header=None)
df['atomic_ie_2'] = pd.read_table(os.path.join(path, "data","matminer", "SecondIonizationEnergy.table"),header=None)
df['atomic_isalkali'] = pd.read_table(os.path.join(path, "data","matminer", "IsAlkali.table"),header=None)
df['atomic_isdblock'] = pd.read_table(os.path.join(path, "data","matminer", "IsDBlock.table"),header=None)
df['atomic_isfblock'] = pd.read_table(os.path.join(path, "data","matminer", "IsFBlock.table"),header=None)
df['atomic_ismetal'] = pd.read_table(os.path.join(path, "data","matminer", "IsMetal.table"),header=None)
df['atomic_ismetalloid'] = pd.read_table(os.path.join(path, "data","matminer", "IsMetalloid.table"),header=None)
df['atomic_isnonmetal'] = pd.read_table(os.path.join(path, "data","matminer", "IsNonmetal.table"),header=None)
df['atomic_ndunf'] = pd.read_table(os.path.join(path, "data","matminer", "NdUnfilled.table"),header=None)
df['atomic_ndval'] = pd.read_table(os.path.join(path, "data","matminer", "NdValence.table"),header=None)
df['atomic_nfunf'] = pd.read_table(os.path.join(path, "data","matminer", "NfUnfilled.table"),header=None)
df['atomic_nfval'] = pd.read_table(os.path.join(path, "data","matminer", "NfValence.table"),header=None)
df['atomic_npunf'] = pd.read_table(os.path.join(path, "data","matminer", "NpUnfilled.table"),header=None)
df['atomic_npval'] = pd.read_table(os.path.join(path, "data","matminer", "NpValence.table"),header=None)
df['atomic_nsunf'] = pd.read_table(os.path.join(path, "data","matminer", "NsUnfilled.table"),header=None)
df['atomic_nsval'] = pd.read_table(os.path.join(path, "data","matminer", "NsValence.table"),header=None)
df['atomic_nunf'] = pd.read_table(os.path.join(path, "data","matminer", "NUnfilled.table"),header=None)
df['atomic_nval'] = pd.read_table(os.path.join(path, "data","matminer", "NValence.table"),header=None)
df['atomic_oxstates'] = oxistates()
df['atomic_phi'] = pd.read_table(os.path.join(path, "data","matminer", "phi.table"),header=None)
df['atomic_pol'] = pd.read_table(os.path.join(path, "data","matminer", "Polarizability.table"),header=None)
df['atomic_pp_r_d'] = pd.read_table(os.path.join(path, "data","matminer", "ZungerPP-r_d.table"),header=None)
df['atomic_pp_r_p'] = pd.read_table(os.path.join(path, "data","matminer", "ZungerPP-r_p.table"),header=None)
df['atomic_pp_r_pi'] = pd.read_table(os.path.join(path, "data","matminer", "ZungerPP-r_pi.table"),header=None)
df['atomic_pp_r_s'] = pd.read_table(os.path.join(path, "data","matminer", "ZungerPP-r_s.table"),header=None)
df['atomic_pp_r_sig'] = pd.read_table(os.path.join(path, "data","matminer", "ZungerPP-r_sigma.table"),header=None)
df['atomic_radius'] = pd.read_table(os.path.join(path, "data","matminer", "AtomicRadius.table"),header=None)
df['atomic_sgn'] = pd.read_table(os.path.join(path, "data","matminer", "SpaceGroupNumber.table"),header=None)
df['atomic_vdw_radius'] = pd.read_table(os.path.join(path, "data","matminer", "VdWRadius.table"),header=None)
df['atomic_volume'] = pd.read_table(os.path.join(path, "data","matminer", "AtomicVolume.table"),header=None)
df['atomic_weight'] = pd.read_table(os.path.join(path, "data","matminer", "AtomicWeight.table"),header=None)
df['atomic_ws3'] = pd.read_table(os.path.join(path, "data","matminer", "n_ws^third.table"),header=None)
df['boiling_temp'] = pd.read_table(os.path.join(path, "data","matminer", "BoilingT.table"),header=None)
df['bulk_modulus'] = pd.read_table(os.path.join(path, "data","matminer", "BulkModulus.table"),header=None)
df['covalent_radius'] = pd.read_table(os.path.join(path, "data","matminer", "CovalentRadius.table"),header=None)
df['gs_bandgap'] = pd.read_table(os.path.join(path, "data","matminer", "GSbandgap.table"),header=None)
df['gs_bcclatparam'] = pd.read_table(os.path.join(path, "data","matminer", "GSestBCClatcnt.table"),header=None)
df['gs_energy_pa'] = pd.read_table(os.path.join(path, "data","matminer", "GSenergy_pa.table"),header=None)
df['gs_fcclatparam'] = pd.read_table(os.path.join(path, "data","matminer", "GSestFCClatcnt.table"),header=None)
df['gs_mag_mom'] = pd.read_table(os.path.join(path, "data","matminer", "GSmagmom.table"),header=None)
df['gs_volume_pa'] = pd.read_table(os.path.join(path, "data","matminer", "GSvolume_pa.table"),header=None) 
df['melting_point'] = pd.read_table(os.path.join(path, "data","matminer", "MeltingT.table"),header=None)
df['mendeleev_no'] = pd.read_table(os.path.join(path, "data","matminer", "MendeleevNumber.table"),header=None)
df['micracle_radius'] =pd.read_table(os.path.join(path, "data","matminer", "MiracleRadius.table"),header=None)
df['molar_volume'] = pd.read_table(os.path.join(path, "data","matminer", "MolarVolume.table"),header=None)
df['periodict_column'] = pd.read_table(os.path.join(path, "data","matminer", "Column.table"),header=None)
df['periodict_row'] = pd.read_table(os.path.join(path, "data","matminer", "Row.table"),header=None)
df['shear_mod'] = pd.read_table(os.path.join(path, "data","matminer", "ShearModulus.table"),header=None)
df['thermal_cond'] = pd.read_table(os.path.join(path, "data","matminer", "ThermalConductivity.table"),header=None)
df['thermal_cond_log'] = pd.read_table(os.path.join(path, "data","matminer", "LogThermalConductivity.table"),header=None)
df['atomic_ie'] = ionizationen()


df.replace(to_replace = ['Missing','Missing["NotAvailable"]','Missing["Unknown"]','None','NA'], value = np.nan,inplace=True )


#Section instantiation and its objects attributes are filled. This will create 118 objects unique ( atomic element symbol) which can be used to access its properties

objs = []
for i in df['atomic_element_symbol']:
    matminer = atomic_properties_matminer()
    prop = matminer.m_create(metadata)
    objs.append(matminer)

count = 0
for obj in objs:
        obj.section_matmin_metadata.source = 'Magpie'
        obj.section_matmin_metadata.atomic_spin_setting = 'NA'
        obj.section_matmin_metadata.atomic_method = 'NA'
        obj.section_matmin_metadata.atomic_basis_set = 'NA'
        obj.atomic_ICSD_vol = float(df.iloc[count,df.columns.get_loc("atomic_ICSD_vol")])
        obj.atomic_cpmass = float(df.iloc[count,df.columns.get_loc("atomic_cpmass")])
        obj.atomic_cpmolar = float(df.iloc[count,df.columns.get_loc("atomic_cpmolar")])
        obj.atomic_density = float(df.iloc[count,df.columns.get_loc("atomic_density")])
        obj.atomic_ea = float(df.iloc[count,df.columns.get_loc("atomic_ea")])
        obj.atomic_element_symbol = str(df.iloc[count,df.columns.get_loc("atomic_element_symbol")])
        obj.atomic_en = float(df.iloc[count,df.columns.get_loc("atomic_en")])
        obj.atomic_en_allen = float(df.iloc[count,df.columns.get_loc("atomic_en_allen")])
        obj.atomic_hfu = float(df.iloc[count,df.columns.get_loc("atomic_hfu")])
        obj.atomic_hhip = float(df.iloc[count,df.columns.get_loc("atomic_hhip")])
        obj.atomic_hhir = float(df.iloc[count,df.columns.get_loc("atomic_hhir")])
        obj.atomic_hvap = float(df.iloc[count,df.columns.get_loc("atomic_hvap")])
        obj.atomic_ie = df.iloc[count,df.columns.get_loc("atomic_ie")]
        obj.atomic_ie_1 = float(df.iloc[count,df.columns.get_loc("atomic_ie_1")])
        obj.atomic_ie_2 = float(df.iloc[count,df.columns.get_loc("atomic_ie_2")])
        obj.atomic_isalkali = float(df.iloc[count,df.columns.get_loc("atomic_isalkali")])
        obj.atomic_isdblock = float(df.iloc[count,df.columns.get_loc("atomic_isdblock")])
        obj.atomic_isfblock = float(df.iloc[count,df.columns.get_loc("atomic_isfblock")])
        obj.atomic_ismetal = float(df.iloc[count,df.columns.get_loc("atomic_ismetal")])
        obj.atomic_ismetalloid = float(df.iloc[count,df.columns.get_loc("atomic_ismetalloid")])
        obj.atomic_isnonmetal = float(df.iloc[count,df.columns.get_loc("atomic_isnonmetal")])
        obj.atomic_ndunf = float(df.iloc[count,df.columns.get_loc("atomic_ndunf")])
        obj.atomic_ndval = float(df.iloc[count,df.columns.get_loc("atomic_ndval")])
        obj.atomic_nfunf = float(df.iloc[count,df.columns.get_loc("atomic_nfunf")])
        obj.atomic_nfval = float(df.iloc[count,df.columns.get_loc("atomic_nfval")])
        obj.atomic_npunf = float(df.iloc[count,df.columns.get_loc("atomic_npunf")])
        obj.atomic_npval = float(df.iloc[count,df.columns.get_loc("atomic_npval")])
        obj.atomic_nsunf = float(df.iloc[count,df.columns.get_loc("atomic_nsunf")])
        obj.atomic_nsval = float(df.iloc[count,df.columns.get_loc("atomic_nsval")])
        obj.atomic_number = int(df.iloc[count,df.columns.get_loc("atomic_number")])
        obj.atomic_nunf = float(df.iloc[count,df.columns.get_loc("atomic_nunf")])
        obj.atomic_nval = float(df.iloc[count,df.columns.get_loc("atomic_nval")])
        obj.atomic_oxstates = df.iloc[count,df.columns.get_loc("atomic_oxstates")]
        obj.atomic_phi = float(df.iloc[count,df.columns.get_loc("atomic_phi")])
        obj.atomic_pol = float(df.iloc[count,df.columns.get_loc("atomic_pol")])
        obj.atomic_pp_r_d = float(df.iloc[count,df.columns.get_loc("atomic_pp_r_d")])
        obj.atomic_pp_r_p = float(df.iloc[count,df.columns.get_loc("atomic_pp_r_p")])
        obj.atomic_pp_r_pi = float(df.iloc[count,df.columns.get_loc("atomic_pp_r_pi")])
        obj.atomic_pp_r_s = float(df.iloc[count,df.columns.get_loc("atomic_pp_r_s")])
        obj.atomic_pp_r_sig = float(df.iloc[count,df.columns.get_loc("atomic_pp_r_sig")])
        obj.atomic_radius = float(df.iloc[count,df.columns.get_loc("atomic_radius")])
        obj.atomic_sgn = float(df.iloc[count,df.columns.get_loc("atomic_sgn")])
        obj.atomic_vdw_radius = float(df.iloc[count,df.columns.get_loc("atomic_vdw_radius")])
        obj.atomic_volume = float(df.iloc[count,df.columns.get_loc("atomic_volume")])
        obj.atomic_weight = float(df.iloc[count,df.columns.get_loc("atomic_weight")])
        obj.atomic_ws3 = float(df.iloc[count,df.columns.get_loc("atomic_ws3")])
        obj.boiling_temp = float(df.iloc[count,df.columns.get_loc("boiling_temp")])
        obj.bulk_modulus = float(df.iloc[count,df.columns.get_loc("bulk_modulus")])
        obj.boiling_temp = float(df.iloc[count,df.columns.get_loc("boiling_temp")])
        obj.covalent_radius = float(df.iloc[count,df.columns.get_loc("covalent_radius")])
        obj.gs_bandgap = float(df.iloc[count,df.columns.get_loc("gs_bandgap")])
        obj.gs_bcclatparam = float(df.iloc[count,df.columns.get_loc("gs_bcclatparam")])
        obj.gs_mag_mom = float(df.iloc[count,df.columns.get_loc("gs_mag_mom")])
        obj.gs_volume_pa = float(df.iloc[count,df.columns.get_loc("gs_volume_pa")])
        obj.melting_point = float(df.iloc[count,df.columns.get_loc("melting_point")])
        obj.mendeleev_no = float(df.iloc[count,df.columns.get_loc("mendeleev_no")])
        obj.micracle_radius = float(df.iloc[count,df.columns.get_loc("micracle_radius")])
        obj.molar_volume = float(df.iloc[count,df.columns.get_loc("molar_volume")])
        obj.periodict_column = int(df.iloc[count,df.columns.get_loc("periodict_column")])
        obj.periodict_row = int(df.iloc[count,df.columns.get_loc("periodict_row")])
        obj.shear_mod = float(df.iloc[count,df.columns.get_loc("shear_mod")])
        obj.thermal_cond = float(df.iloc[count,df.columns.get_loc("thermal_cond")])
        obj.thermal_cond_log = float(df.iloc[count,df.columns.get_loc("thermal_cond_log")])
        count+=1
        
atomic_element_symbol = df['atomic_element_symbol'].values
for count, symbol in enumerate(atomic_element_symbol):
    globals()[symbol] = objs[count]



def definition(abc):
    '''
    This function can be used to access quantity definitions for each element features accessible
    '''
    if hasattr(atomic_properties_matminer,abc):
        return print(getattr(atomic_properties_matminer,abc).__doc__)
    elif hasattr(metadata,abc):
        return print(getattr(metadata,abc).__doc__)
    else:
        return print('Check the input feature name. Either it is wrong or requested feature does not exist from magpie source') 


def symbol(abc):
    '''
This function utilty is, it can be called to access element properties based on its symbol in periodic table. Can be usefull if one wants to acess particular property of multiple elements at once
    '''
    return globals()[abc]

