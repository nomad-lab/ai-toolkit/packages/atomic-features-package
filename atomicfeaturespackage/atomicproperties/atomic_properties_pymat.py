#!/usr/bin/env python
# coding: utf-8

''' 
Module : atomic_properties_pymat


This module with help of  atomic_properties_pymat class from metainfo module instantiates objects with atomic element symbol as identifier to access atomic features available 
'''


from atomicfeaturespackage.metainfo.metainfo import atomic_properties_pymat,metadata
import pandas as pd
import numpy as np
import urllib, json, re, typing
from pandas.io.json import json_normalize
import os
import warnings
warnings.filterwarnings("ignore", category=DeprecationWarning)

path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","pymat", "periodic_table.json")
df1 = pd.read_json(path_new)

# Flatten the nested dictionary of Shannon radii feature into simple key-value dictionary
for elem in df1.columns:
    if type(df1.loc['Shannon radii'][elem]) == dict:
        df1.loc['Shannon radii'][elem] = (json_normalize(df1.loc['Shannon radii'][elem]).to_dict(orient='records')[0])


# Replace the missing spin data for Shannon radii keys into a dictionary

repl_dict ={}
for elem in df1.columns:
    if type(df1.loc['Shannon radii'][elem]) == dict:
        try:
            df1.loc['Shannon radii'][elem].keys()
            for key in df1.loc['Shannon radii'][elem].keys():
                if re.search(r"(.High Spin.)", key):
                    new_key = re.sub(r"(.High Spin.)",'.high_spin.',key)
                    #print(key)
                elif re.search(r"(.Low Spin.)", key):
                    new_key = re.sub(r"(.Low Spin.)",'.low_spin.',key)
                    #print(key)
                else: 
                    new_key = re.sub(r"(\.\.)",'.no_spin.',key)

                    #new_key = re.sub(r"(\.\.)",'.No_Spin.',key)
                repl_dict.update({key : new_key})
        except AttributeError:
            continue

#This rename_keys() function is called to replace keys of each element            
def rename_keys(d, keys):
    d = dict([(keys.get(k), v) for k, v in d.items()])
    return d

for elem in df1.columns:
    if type(df1.loc['Shannon radii'][elem]) == dict:
        test = rename_keys(df1.loc['Shannon radii'][elem], repl_dict)
        df1.loc['Shannon radii'][elem] = test   

# Dataframe manulpulation to sort the data from lowest to highest atomic number

df2 = df1.transpose()
df2.reset_index();
df3 = df2.sort_values('Atomic no')
df3.reset_index();


#Remove the unwanted text and units from dataframe columns (Cleaning the dataset)

df3['Coefficient of linear thermal expansion'] = df3['Coefficient of linear thermal expansion'].str.replace(r"(?: x10<sup>-6</sup>K<sup>-1</sup>)", '')
df3['Boiling point'] = df3['Boiling point'].str.replace(r"(?: K)", '')
df3['Brinell hardness'] = df3['Brinell hardness'].str.replace(r"(?: MN m<sup>-2</sup>)", '')
df3['Bulk modulus'] = df3['Bulk modulus'].str.replace(r"(?: GPa)|(?:\([^()]*\)GPa)", '')
df3['Youngs modulus'] = df3['Youngs modulus'].str.replace(r"(?: GPa)", '')
df3['Critical temperature'] = df3['Critical temperature'].str.replace(r"(?:K)", '')
df3['Density of solid'] = df3['Density of solid'].str.replace(r"(?: kg m<sup>-3</sup>)", '')
df3['Electrical resistivity'] = df3['Electrical resistivity'].str.replace(r"(?: 10<sup>-8</sup> &Omega; m)|(?:; 10<sup>15</sup>)|(?:; 10<sup>12</sup>)|(?:; 10<sup>18</sup>)|(?:; 10<sup>23</sup>)|(?:; 10<sup>10</sup>)|(?:&gt10<sup>-8</sup> &Omega; m)|(?:10<sup>-8</sup> &Omega; m)", '')
df3['Liquid range'] = df3['Liquid range'].str.replace(r"(?: K)", '')
df3['Melting point'] = df3['Melting point'].str.replace(r"(?: K)", '')
df3['Molar volume'] = df3['Molar volume'].str.replace(r"(?: cm<sup>3</sup>)", '')
df3['Mineral hardness'] = df3['Mineral hardness'].str.replace(r"(?: \(graphite; diamond is 10.0\)\(no units\))", '')
df3['Reflectivity'] = df3['Reflectivity'].str.replace(r"(?:%)", '')
df3['Rigidity modulus'] = df3['Rigidity modulus'].str.replace(r"(?: GPa)", '')
df3['Superconduction temperature'] = df3['Superconduction temperature'].str.replace(r"(?: K)|(?:K)", '')
df3['Thermal conductivity'] = df3['Thermal conductivity'].str.replace(r"(?: W m<sup>-1</sup> K<sup>-1</sup>)|(?:W m<sup>-1</sup> K<sup>-1</sup>)", '')
df3['Velocity of sound'] = df3['Velocity of sound'].str.replace(r"(?: m s<sup>-1</sup>)", '')
df3['Vickers hardness'] = df3['Vickers hardness'].str.replace(r"(?: MN m<sup>-2</sup>)", '')
df3['Electronic structure'] = df3['Electronic structure'].str.replace(r"(?:<sup>)|(?:<\/sup>)|(?:\([^()]*\))", '')

df3 = df3.replace(["no data","no data ","&gt"],np.nan)
df4 = df3.reset_index()
df4.rename(columns={"index": "Element Symbol"},inplace = True)
df4.drop('iupac_ordering', axis=1, inplace=True)


#Fill the empty features with np.nan values
df_clean = df4.replace(r'^\s*$', np.nan, regex=True)

# Section instantiation and its objects attributes are filled. This will create 118 objects unique ( atomic element symbol) which can be used to access its properties  

objs = []
for i in df_clean['Element Symbol']:
    pymat = atomic_properties_pymat()
    prop = pymat.m_create(metadata)
    objs.append(pymat)

count = 0
for obj in objs:
        obj.section_pymat_metadata.source = 'Pymatgen'
        obj.section_pymat_metadata.atomic_spin_setting = 'NA'
        obj.atomic_number = int(df_clean.iloc[count,df_clean.columns.get_loc("Atomic no")])
        obj.atomic_element_symbol = str(df_clean.iloc[count,df_clean.columns.get_loc("Element Symbol")])
        obj.section_pymat_metadata.atomic_method = 'NA'
        obj.section_pymat_metadata.atomic_basis_set = 'NA'
        obj.atomic_mass = float(df_clean.iloc[count,df_clean.columns.get_loc("Atomic mass")])
        obj.atomic_radius = float(df_clean.iloc[count,df_clean.columns.get_loc("Atomic radius")])
        obj.atomic_radius_calculated = float(df_clean.iloc[count,df_clean.columns.get_loc("Atomic radius calculated")])
        obj.atomic_orbitals = df_clean.iloc[count,df_clean.columns.get_loc("Atomic orbitals")]
        obj.boiling_point = float(df_clean.iloc[count,df_clean.columns.get_loc("Boiling point")])
        obj.brinell_hardness = float(df_clean.iloc[count,df_clean.columns.get_loc("Brinell hardness")])
        obj.bulk_modulus = df_clean.iloc[count,df_clean.columns.get_loc("Bulk modulus")]
        obj.coeff_olte = float(df_clean.iloc[count,df_clean.columns.get_loc("Coefficient of linear thermal expansion")])
        obj.common_ox_states = df_clean.iloc[count,df_clean.columns.get_loc("Common oxidation states")]
        obj.critical_temperature = float(df_clean.iloc[count,df_clean.columns.get_loc("Critical temperature")])
        obj.density_of_solid = float(df_clean.iloc[count,df_clean.columns.get_loc("Density of solid")])
        obj.electrical_resistivity = df_clean.iloc[count,df_clean.columns.get_loc("Electrical resistivity")]
        obj.electronic_structure = df_clean.iloc[count,df_clean.columns.get_loc("Electronic structure")]
        obj.ionic_radii = df_clean.iloc[count,df_clean.columns.get_loc("Ionic radii")]
        obj.liquid_range = float(df_clean.iloc[count,df_clean.columns.get_loc("Liquid range")])
        obj.melting_point = df_clean.iloc[count,df_clean.columns.get_loc("Melting point")]
        obj.mendeleev_no = float(df_clean.iloc[count,df_clean.columns.get_loc("Mendeleev no")])
        obj.mineral_hardness = df_clean.iloc[count,df_clean.columns.get_loc("Mineral hardness")]
        obj.molar_volume = float(df_clean.iloc[count,df_clean.columns.get_loc("Molar volume")])
        obj.atomic_element_name = str(df_clean.iloc[count,df_clean.columns.get_loc("Name")])
        obj.oxidation_states = df_clean.iloc[count,df_clean.columns.get_loc("Oxidation states")]
        obj.poisson_ratio = float(df_clean.iloc[count,df_clean.columns.get_loc("Poissons ratio")])
        obj.atomic_refelctivity = float(df_clean.iloc[count,df_clean.columns.get_loc("Reflectivity")])
        obj.atomic_refractive_index = df_clean.iloc[count,df_clean.columns.get_loc("Refractive index")]
        obj.rigidity_modulus = float(df_clean.iloc[count,df_clean.columns.get_loc("Rigidity modulus")])
        obj.shannon_radii = df_clean.iloc[count,df_clean.columns.get_loc("Shannon radii")]
        obj.thermal_cond = df_clean.iloc[count,df_clean.columns.get_loc("Thermal conductivity")]
        obj.van_der_waals_rad = float(df_clean.iloc[count,df_clean.columns.get_loc("Van der waals radius")])
        obj.velocity_of_sound = float(df_clean.iloc[count,df_clean.columns.get_loc("Velocity of sound")])
        obj.vickers_hardness = float(df_clean.iloc[count,df_clean.columns.get_loc("Vickers hardness")])
        obj.x = float(df_clean.iloc[count,df_clean.columns.get_loc("X")])
        obj.youngs_modulus = float(df_clean.iloc[count,df_clean.columns.get_loc("Youngs modulus")])
        obj.metallic_radius = float(df_clean.iloc[count,df_clean.columns.get_loc("Metallic radius")])
        obj.iupac_ordering = float(df_clean.iloc[count,df_clean.columns.get_loc("IUPAC ordering")])
        obj.icsd_oxd_states = df_clean.iloc[count,df_clean.columns.get_loc("ICSD oxidation states")]
        obj.supercond_temp = df_clean.iloc[count,df_clean.columns.get_loc("Superconduction temperature")]
        obj.nmr_quadrapole_mom = df_clean.iloc[count,df_clean.columns.get_loc("NMR Quadrupole Moment")]
        obj.max_oxd_state = float(df_clean.iloc[count,df_clean.columns.get_loc("Max oxidation state")])
        obj.min_oxd_state = float(df_clean.iloc[count,df_clean.columns.get_loc("Min oxidation state")])
        obj.ionic_radii_hs = df_clean.iloc[count,df_clean.columns.get_loc("Ionic radii hs")]
        obj.ionic_radii_ls = df_clean.iloc[count,df_clean.columns.get_loc("Ionic radii ls")]
        count+=1
        
atomic_element_symbol = df_clean['Element Symbol'].values
for count, symbol in enumerate(atomic_element_symbol):
    globals()[symbol] = objs[count]

def definition(abc):
    '''
    This function can be used to access quantity definitions for each element features accessible
    '''
    
    if hasattr(atomic_properties_pymat,abc):
        return print(getattr(atomic_properties_pymat,abc).__doc__)
    elif hasattr(metadata,abc):
        return print(getattr(metadata,abc).__doc__)
    else:
        return print('Check the input feature name. Either it is wrong or requested feature does not exist from pymatgen source') 

def symbol(abc):
    '''
    This function utilty is, it can be called to access element properties based on its symbol in periodic table. Can be usefull if want to acess   particular property of multiple elements at once
    '''

    return globals()[abc]

# This method_list object could be called after importing this module so one can easily see all the quantities accessible.
method_list = [method for method in dir(atomic_properties_pymat) if (method.startswith('m_') or method.startswith('_') or method.startswith('__') or method.startswith('val') or method.startswith('section_') or method.startswith('get'))  is False]
