#!/usr/bin/env python
# coding: utf-8

'''
Module : atomic_properties_webele

This module with help of  atomic_properties_webele class from metainfo module instantiates objects with atomic element symbol as identifier to access atomic features available 
'''


from atomicfeaturespackage.metainfo.metainfo import atomic_properties_webele,metadata
import pandas as pd
import numpy as np
from mendeleev import element
import os
from ast import literal_eval
import re


# This method_list object could be called after importing this module so one can easily see all the quantities accessible.

method_list = [method for method in dir(atomic_properties_webele) if (method.startswith('m_') or method.startswith('_') or method.startswith('__') or method.startswith('val') or method.startswith('section_') or method.startswith('get'))  is False]



path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","webele", "Webele.csv")
df_Webele = pd.read_csv(path_new,converters={'Electron_bin_En': literal_eval,'Eff_Nuc_Char':literal_eval,
                                            'Ionisation_En':literal_eval})


path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","webele", "atomic_sizes.csv")
df_atomic_sizes = pd.read_csv(path_new,converters={'atom_radii': literal_eval,
                                                    'orbital_radii':literal_eval,
                                                    'ionic_radii':literal_eval,
                                                   'paul_ionic_radii':literal_eval})


path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","webele", "cryst_struct.csv")
df_crys_struct = pd.read_csv(path_new,converters={'cryst_struct': literal_eval})




path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","webele", "electronegativity.csv")
df_en = pd.read_csv(path_new,converters={'electronegativity': literal_eval})




df_Webele=df_Webele.set_index('Unnamed: 0')


path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","webele", "isotopes.csv")
df_iso = pd.read_csv(path_new,converters={'nat_isotopes': literal_eval,'radiosotope_data':literal_eval,
                                         'nmr_properties':literal_eval})


path = os.path.abspath(os.path.join(os.path.dirname(__file__),".."))
path_new = os.path.join(path, "data","webele", "thermo_chem.csv")
df_therm = pd.read_csv(path_new,converters={'thermo_chem': literal_eval})


objs = []
for i in df_Webele['Element_symbol']:
    webele = atomic_properties_webele()
    prop = webele.m_create(metadata)
    objs.append(webele)



for i in df_Webele['Electron_bin_En']:
    for key,value in i.items():
        i.update({key:value.replace(r" [2]",'').replace(r" [1]",'').replace(r" [3]",'').
                 replace(r" [2, values derived from reference 1]",'').
                  replace(r" [2, one-particle approximation not valid owing to short core-hole lifetime]",'').
    replace(r" [2, one-particle approximation not valid owing to short core-hole lifetime, values derived from reference 1]",'').
                 replace(r" [3, one-particle approximation not valid owing to short core-hole lifetime]",'')})
        
for i in df_Webele['Ionisation_En']:
    for key,value in i.items():
        i.update({key:value.replace(r" (calculated)",'').replace(r" (inferred)",'')})
        
for i in df_Webele['Eff_Nuc_Char']:
    for key,value in i.items():
        i.update({key:value.replace(r"(no data)",'')})




for i in range(len(df_atomic_sizes)):
    k=df_atomic_sizes.loc[i,'atom_radii']['Covalent radius (2008 values)']
    df_atomic_sizes.loc[i,'atom_radii']['Covalent radius (2008 values)']=re.sub(r"( \(.*?\))|(-)", '', k)
    l=df_atomic_sizes.loc[i,'atom_radii']['Covalent radius (empirical)']
    df_atomic_sizes.loc[i,'atom_radii']['Covalent radius (empirical)']=re.sub(r"( \(.*?\))|(-)", '', l)
    
    m=df_atomic_sizes.loc[i,'atom_radii']['Atomic radius (empirical)']
    df_atomic_sizes.loc[i,'atom_radii']['Atomic radius (empirical)']=re.sub(r"( \(.*?\))|(-)", '', m)
    
    n=df_atomic_sizes.loc[i,'atom_radii']['Atomic radius (calculated)']
    df_atomic_sizes.loc[i,'atom_radii']['Atomic radius (calculated)']=re.sub(r"( \(.*?\))|(-)", '', n)
    
    o=df_atomic_sizes.loc[i,'atom_radii']['van der Waals radius']
    df_atomic_sizes.loc[i,'atom_radii']['van der Waals radius']=re.sub(r"( \(.*?\))|(-)", '', o)




# replace empty values with np.nan
for i in range(len(df_atomic_sizes)):
    a=df_atomic_sizes.loc[i,'paul_ionic_radii']
    for k in df_atomic_sizes.loc[i,'paul_ionic_radii'].keys():
        temp=df_atomic_sizes.loc[i,'paul_ionic_radii'][k]
        df_atomic_sizes.loc[i,'paul_ionic_radii'][k]=re.sub(r"( \(.*?\))|(-)", '', temp)
        if df_atomic_sizes.loc[i,'paul_ionic_radii'][k]=="":
            df_atomic_sizes.loc[i,'paul_ionic_radii'][k]=np.nan

# replace empty values with np.nan           
for i in range(len(df_atomic_sizes)):
    a=df_atomic_sizes.loc[i,'orbital_radii']
    for k in df_atomic_sizes.loc[i,'orbital_radii'].keys():
        temp=df_atomic_sizes.loc[i,'orbital_radii'][k]
        df_atomic_sizes.loc[i,'orbital_radii'][k]=re.sub(r"( \(.*?\))|(-)", '', temp)
        if df_atomic_sizes.loc[i,'orbital_radii'][k]=="":
            df_atomic_sizes.loc[i,'orbital_radii'][k]=np.nan

# replace empty values with np.nan
for i in range(len(df_atomic_sizes)):
    a=df_atomic_sizes.loc[i,'ionic_radii']
    if a=={}:
        df_atomic_sizes.loc[i,'ionic_radii']=np.nan




# extract data for covalent_radius_mol attribute dictionary

df_as_copy = df_atomic_sizes.copy(deep=True)
df_as_copy['atom_radii'] = df_as_copy.atom_radii.apply(dict.copy)


rem_keys= ['Atomic radius (empirical)', 'Atomic radius (calculated)', 
       'Covalent radius (2008 values)','Covalent radius (empirical)', 'van der Waals radius']

for dictionary in df_as_copy['atom_radii']:
    count=0
    for key in dictionary.copy(): # avoid dictionary changed size error during iteration using .copy method
        for k in rem_keys:
            if key==k:
                del dictionary[k]
    count=count+1


df_Webele['Density_of_solid']=df_Webele['Density_of_solid'].str.replace(r"(\(.*?\))", '')
df_Webele['Density_of_solid'] = np.where(df_Webele['Density_of_solid'] == ' ',np.nan, df_Webele['Density_of_solid'])

df_Webele['Molar_vol']=df_Webele['Molar_vol'].str.replace(r"(\(.*?\))", '')
df_Webele['Molar_vol']=np.where(df_Webele['Molar_vol'] == ' ',np.nan, df_Webele['Molar_vol'])

df_Webele['Youngs_mod']=df_Webele['Youngs_mod'].str.replace(r"(\(.*?\))", '')
df_Webele['Youngs_mod']=np.where(df_Webele['Youngs_mod'] == ' ',np.nan, df_Webele['Youngs_mod'])

df_Webele['Rigidity_mod']=df_Webele['Rigidity_mod'].str.replace(r"(\(.*?\))", '')
df_Webele['Rigidity_mod']=np.where(df_Webele['Rigidity_mod'] == ' ',np.nan, df_Webele['Rigidity_mod'])

df_Webele['Bulk_mod']=df_Webele['Bulk_mod'].str.replace(r"(\(.*?\))", '')
df_Webele['Bulk_mod']=np.where(df_Webele['Bulk_mod'] == ' ',np.nan, df_Webele['Bulk_mod'])

df_Webele['Poissons_ratio']=df_Webele['Poissons_ratio'].str.replace(r"(\(.*?\))", '')
df_Webele['Poissons_ratio']=np.where(df_Webele['Poissons_ratio'] == '  ',np.nan, df_Webele['Poissons_ratio'])

df_Webele['Mineral_hardness']=df_Webele['Mineral_hardness'].str.replace(r"(\(.*?\))", '')
df_Webele['Mineral_hardness']=np.where(df_Webele['Mineral_hardness'] == '  ',np.nan, df_Webele['Mineral_hardness'])

df_Webele['Brinell_hardness']=df_Webele['Brinell_hardness'].str.replace(r"(\(.*?\))", '')
df_Webele['Brinell_hardness']=np.where(df_Webele['Brinell_hardness'] == ' ',np.nan, df_Webele['Brinell_hardness'])

df_Webele['Vickers_hardness']=df_Webele['Vickers_hardness'].str.replace(r"(\(.*?\))", '')
df_Webele['Vickers_hardness']=np.where(df_Webele['Vickers_hardness'] == ' ',np.nan, df_Webele['Vickers_hardness'])

df_Webele['Electrical_resistivity']=df_Webele['Electrical_resistivity'].str.replace(r"(\(.*?\))|(about )|( - direction dependent)|(> )|( high)", '')
df_Webele['Electrical_resistivity']=np.where(df_Webele['Electrical_resistivity'] == ' ',np.nan, df_Webele['Electrical_resistivity'])

df_Webele['Thermal_conductivity']=df_Webele['Thermal_conductivity'].str.replace(r"(\(.*?\))", '')
df_Webele['Thermal_conductivity']=np.where(df_Webele['Thermal_conductivity'] == ' ',np.nan, df_Webele['Thermal_conductivity'])

df_Webele['Coeff_of_lte']=df_Webele['Coeff_of_lte'].str.replace(r"(\(.*?\))", '')
df_Webele['Coeff_of_lte']=np.where(df_Webele['Coeff_of_lte'] == ' ',np.nan, df_Webele['Coeff_of_lte'])

df_Webele['Reflectivity']=df_Webele['Reflectivity'].str.replace(r"(\(.*?\))", '')
df_Webele['Reflectivity']=np.where(df_Webele['Reflectivity'] == ' ',np.nan, df_Webele['Reflectivity'])

df_Webele['Refractive_index']=df_Webele['Refractive_index'].str.replace(r"(\(.*?\))", '')
df_Webele['Refractive_index']=np.where(df_Webele['Refractive_index'] == '  ',np.nan, df_Webele['Refractive_index'])

df_Webele['Velocity_of_sound']=df_Webele['Velocity_of_sound'].str.replace(r"(\(.*?\))", '')
df_Webele['Velocity_of_sound']=np.where(df_Webele['Velocity_of_sound'] == ' ',np.nan, df_Webele['Velocity_of_sound'])

df_Webele['Electron_aff']=df_Webele['Electron_aff'].str.replace(r"(\(.*?\))|( http://dx.doi.org/10.1088/0953-4075/34/14/102)|(>= )|(is )", '')
df_Webele['Electron_aff']=np.where(df_Webele['Electron_aff'] == ' ',np.nan, df_Webele['Electron_aff'])



for i in range(len(df_crys_struct)):
    a=df_crys_struct.loc[i,'cryst_struct']['Space group']
    if a=="" or a=="unknown":
        df_crys_struct.loc[i,'cryst_struct']['Space group']='-'
    b=df_crys_struct.loc[i,'cryst_struct']['Space group number']
    if b=="" or b=="unknown":
        df_crys_struct.loc[i,'cryst_struct']['Space group number']=np.nan
    c=df_crys_struct.loc[i,'cryst_struct']['Structure']
    if c=="" or c=="unknown":
        df_crys_struct.loc[i,'cryst_struct']['Structure']='-'



df_crys_struct['atomic_cellparam']=''
df_crys_struct['atomic_cellparam'] = df_crys_struct['cryst_struct'].copy(deep=True)
df_crys_struct['atomic_cellparam'] = df_crys_struct['cryst_struct'].apply(dict.copy)


rem_keys= ['Space group','Space group number','Structure']

for dictionary in df_crys_struct['atomic_cellparam']:
    count=0
    for key in dictionary.copy(): # avoid dictionary changed size error during iteration using .copy method
        for k in rem_keys:
            if key==k:
                del dictionary[k]
    count=count+1



for i in df_crys_struct['atomic_cellparam']:
    for k,v in i.items():
        #print(v)
        if v=="" or v=="unknown":
            i[k]=np.nan


for i in df_en['electronegativity']:
    for k,v in i.items():
        if k=='Allen electronegativity':
            if i[k]=='(no data)':
                i[k]=np.nan
                #print(k,i[k])
            else:
                i[k]==float(i[k])
                #print(k,i[k])
        if k=='Pauling electronegativity':
            if i[k]=='(no data)':
                i[k]=np.nan
                #print(k,i[k])
            else:
                i[k]==float(i[k])
                #print(k,i[k])
        if k=='Allred Rochow electronegativity':
            if i[k]=='(no data)':
                i[k]=np.nan
                #print(k,i[k])
            else:
                i[k]==float(i[k])
                #print(k,i[k])
        if k=='Sanderson electronegativity':
            if i[k]=='(no data)':
                i[k]=np.nan
                #print(k,i[k])
            else:
                i[k]==float(i[k])
                #print(k,i[k])
        if k=='Mulliken-Jaffe electronegativity':
            if i[k]=='(no data)':
                i[k]=np.nan
                #print(k,i[k])
            else:
                pass
                #print(k,i[k])


# Clean isotopes data

for i in df_iso['nat_isotopes']:
    #print(i.values())
    for k,v in i.items():
        #pass
        for key,value in v.items():
            if value=='no data':
                v[key]=np.nan
                
for i in df_iso['radiosotope_data']:
    #print(i.values())
    for k,v in i.copy().items():
        #pass
        #print(k)
        #print(v.values())
        try:
            for key,value in v.items():
                #print(value)
                if value=='no data':
                    v[key]=np.nan
        except AttributeError:
            del i[k]
            #print("Hello")


# Clean and structure nmr data
# Create new nmr_mod column to organize nmr data

df_iso['nmr_mod']=""

# Create structured nmr_mod named python dict to store nmr data systematically
nmr_mod={}
for i in df_Webele.index:
    nmr_mod.update({i:{}})
    
# Store isotope names in a list and remove missing data values from df_iso['nmr_properties'] dataframe

Keys= list(df_iso.loc[0,'nmr_properties'].keys())
for k in Keys:
    for i in df_iso['nmr_properties']:
        for ele in i[k]:
            try:
                while ele == 'no data':
                    i[k].remove(ele)
                while ele == 'none':
                    i[k].remove(ele)
            except ValueError:
                pass
    
#Create dictionary keys
for row,col in df_iso.iterrows():
    for k in nmr_mod.keys():
        if col[0]==k:
            #for ele in range(len(col[3]['Isotope'])):
            for nam in col[3]['Isotope']:
                nmr_mod[k].update({'Isotope_{}'.format(nam):{}})
                row=row+1



#Create nested dictionary keys inside nmr_data dictionary
for row,col in df_iso.iterrows():
    for k in nmr_mod.keys():
        if col[0]==k:
            for iso in nmr_mod[k].keys():
                for nam in col[3]['Natural abundance /%']:
                    nmr_mod[k][iso].update({'Natural abundance %':"",
                                            "Spin (I)":"",
                                           'Frequency relative to 1H = 100 (MHz)':"",
                                            'Receptivity_DP_relative_to_1H = 1.00':"",
                                           'Receptivity_DC_relative_to_13C = 1.00':"",
                                           'Magnetogyric_ratio (γ)':"",
                                           'Magnetic_moment μ (μN)':"",
                                           'Nuclear_quad_mom (Q/millibarn)':"",
                                           'Line_width_factor 1056 l (m4)':""})
                row=row+1

# Finally fill data in the dictionary
for row,col in df_iso.iterrows():
    for k in nmr_mod.keys():
        if col[0]==k:
            ind=0
            for iso in nmr_mod[k].keys():
                try:
                    nmr_mod[k][iso].update({'Natural abundance %':col[3]['Natural abundance /%'][ind]})
                    nmr_mod[k][iso].update({'Spin (I)':col[3]['Spin (I)'][ind]})
                    nmr_mod[k][iso].update({'Frequency relative to 1H = 100 (MHz)':col[3]['Frequency relative to 1H = 100 (MHz)'][ind]})
                    nmr_mod[k][iso].update({'Receptivity_DP_relative_to_1H = 1.00':col[3]['Receptivity, DP, relative to 1H = 1.00'][ind]})
                    nmr_mod[k][iso].update({'Receptivity_DC_relative_to_13C = 1.00':col[3]['Receptivity, DC, relative to 13C = 1.00'][ind]})
                    nmr_mod[k][iso].update({'Magnetogyric_ratio (γ)':col[3]['Magnetogyric ratio, γ (107 rad\xa0T‑1\xa0s-1)'][ind]})
                    nmr_mod[k][iso].update({'Magnetic_moment μ (μN)':col[3]['Magnetic moment, μ (μN)'][ind]})
                    nmr_mod[k][iso].update({'Nuclear_quad_mom (Q/millibarn)':col[3]['Nuclear quadrupole moment, Q/millibarn'][ind]})
                    nmr_mod[k][iso].update({'Line_width_factor 1056 l (m4)':col[3]['Line width factor, 1056 l (m4)'][ind]})
                    ind=ind+1
                    row=row+1
                except IndexError:
                    pass
            


# correcting erroneous data filled for some elements in nmr_mod dictionary manually.

nmr_mod['carbon']['Isotope_11C'].update({'Natural abundance %':"",
                                            "Spin (I)":"",
                                           'Frequency relative to 1H = 100 (MHz)':"",
                                            'Receptivity_DP_relative_to_1H = 1.00':"",
                                           'Receptivity_DC_relative_to_13C = 1.00':"",
                                           'Magnetogyric_ratio (γ)':"",
                                           'Magnetic_moment μ (μN)':"",
                                           'Nuclear_quad_mom (Q/millibarn)':'33.27(24)',
                                           'Line_width_factor 1056 l (m4)':""})

nmr_mod['carbon']['Isotope_13C'].update({'Natural abundance %':1.07,
                                            "Spin (I)":"1/2",
                                           'Frequency relative to 1H = 100 (MHz)':25.145020,
                                            'Receptivity_DP_relative_to_1H = 1.00':0.000170,
                                           'Receptivity_DC_relative_to_13C = 1.00':1.00,
                                           'Magnetogyric_ratio (γ)':6.728284,
                                           'Magnetic_moment μ (μN)':1.216613,
                                           'Nuclear_quad_mom (Q/millibarn)':'-',
                                           'Line_width_factor 1056 l (m4)':"-"})

nmr_mod['calcium']['Isotope_41Ca'].update({'Natural abundance %':"",
                                            "Spin (I)":"",
                                           'Frequency relative to 1H = 100 (MHz)':"",
                                            'Receptivity_DP_relative_to_1H = 1.00':"",
                                           'Receptivity_DC_relative_to_13C = 1.00':"",
                                           'Magnetogyric_ratio (γ)':"",
                                           'Magnetic_moment μ (μN)':"",
                                           'Nuclear_quad_mom (Q/millibarn)':'-40.8(8)',
                                           'Line_width_factor 1056 l (m4)':""})

nmr_mod['calcium']['Isotope_43Ca'].update({'Natural abundance %': '0.135',
  'Spin (I)': '7/2',
  'Frequency relative to 1H = 100 (MHz)': '6.730029',
  'Receptivity_DP_relative_to_1H = 1.00': '0.00000868',
  'Receptivity_DC_relative_to_13C = 1.00': '0.0510',
  'Magnetogyric_ratio (γ)': '-1.803069',
  'Magnetic_moment μ (μN)': '-1.494067',
  'Nuclear_quad_mom (Q/millibarn)': '-4.08',
  'Line_width_factor 1056 l (m4)': '2.3'})

nmr_mod['krypton']['Isotope_85Kr'].update({'Natural abundance %':"",
                                            "Spin (I)":"",
                                           'Frequency relative to 1H = 100 (MHz)':"",
                                            'Receptivity_DP_relative_to_1H = 1.00':"",
                                           'Receptivity_DC_relative_to_13C = 1.00':"",
                                           'Magnetogyric_ratio (γ)':"",
                                           'Magnetic_moment μ (μN)':"",
                                           'Nuclear_quad_mom (Q/millibarn)':'507(3)',
                                           'Line_width_factor 1056 l (m4)':""})

nmr_mod['iodine']['Isotope_129I'].update({'Natural abundance %':"",
                                            "Spin (I)":"",
                                           'Frequency relative to 1H = 100 (MHz)':"",
                                            'Receptivity_DP_relative_to_1H = 1.00':"",
                                           'Receptivity_DC_relative_to_13C = 1.00':"",
                                           'Magnetogyric_ratio (γ)':"",
                                           'Magnetic_moment μ (μN)':"",
                                           'Nuclear_quad_mom (Q/millibarn)':'-604(10)',
                                           'Line_width_factor 1056 l (m4)':""})

nmr_mod['lead']['Isotope_207Pb'].update({'Natural abundance %': '22.1',
  'Spin (I)': '1/2',
  'Frequency relative to 1H = 100 (MHz)': '20.920597',
  'Receptivity_DP_relative_to_1H = 1.00': '0.00201',
  'Receptivity_DC_relative_to_13C = 1.00': '11.5',
  'Magnetogyric_ratio (γ)': '5.58046',
  'Magnetic_moment μ (μN)': '1.00906',
  'Nuclear_quad_mom (Q/millibarn)': '-269(165)',
  'Line_width_factor 1056 l (m4)': '-'})

nmr_mod['plutonium']['Isotope_239Pu'].update({'Natural abundance %': '0',
  'Spin (I)': '1/2',
  'Frequency relative to 1H = 100 (MHz)': '3.63',
  'Receptivity_DP_relative_to_1H = 1.00': '-',
  'Receptivity_DC_relative_to_13C = 1.00': '-',
  'Magnetogyric_ratio (γ)': '0.972',
  'Magnetic_moment μ (μN)': '',
  'Nuclear_quad_mom (Q/millibarn)': '-3345(13) [Mössbauer state]',
  'Line_width_factor 1056 l (m4)': ''})

nmr_mod['plutonium']['Isotope_241Pu'].update({'Natural abundance %': '',
  'Spin (I)': '',
  'Frequency relative to 1H = 100 (MHz)': '',
  'Receptivity_DP_relative_to_1H = 1.00': '',
  'Receptivity_DC_relative_to_13C = 1.00': '',
  'Magnetogyric_ratio (γ)': '',
  'Magnetic_moment μ (μN)': '',
  'Nuclear_quad_mom (Q/millibarn)': '5600(200)',
  'Line_width_factor 1056 l (m4)': ''})

nmr_mod['rhodium']['Isotope_100Rh'].update({'Natural abundance %': '',
  'Spin (I)': '',
  'Frequency relative to 1H = 100 (MHz)': '',
  'Receptivity_DP_relative_to_1H = 1.00': '',
  'Receptivity_DC_relative_to_13C = 1.00': '',
  'Magnetogyric_ratio (γ)': '',
  'Magnetic_moment μ (μN)': '',
  'Nuclear_quad_mom (Q/millibarn)': '153 (excited nuclear state)',
  'Line_width_factor 1056 l (m4)': ''})

nmr_mod['rhodium']['Isotope_103Rh'].update({'Natural abundance %': '100',
  'Spin (I)': '1/2',
  'Frequency relative to 1H = 100 (MHz)': '3.172310',
  'Receptivity_DP_relative_to_1H = 1.00': '0.0000317',
  'Receptivity_DC_relative_to_13C = 1.00': '0.181',
  'Magnetogyric_ratio (γ)': '-0.8468',
  'Magnetic_moment μ (μN)': '-0.1531',
  'Nuclear_quad_mom (Q/millibarn)': '-',
  'Line_width_factor 1056 l (m4)': '-'})

nmr_mod['tungsten']['Isotope_182W'].update({'Natural abundance %': '',
  'Spin (I)': '',
  'Frequency relative to 1H = 100 (MHz)': '',
  'Receptivity_DP_relative_to_1H = 1.00': '',
  'Receptivity_DC_relative_to_13C = 1.00': '',
  'Magnetogyric_ratio (γ)': '',
  'Magnetic_moment μ (μN)': '',
  'Nuclear_quad_mom (Q/millibarn)': '-2130(350)  [Mössbauer state]',
  'Line_width_factor 1056 l (m4)': ''})

nmr_mod['tungsten']['Isotope_183W'].update({'Natural abundance %': '14.3',
  'Spin (I)': '1/2',
  'Frequency relative to 1H = 100 (MHz)': '4.166398',
  'Receptivity_DP_relative_to_1H = 1.00': '0.0000107',
  'Receptivity_DC_relative_to_13C = 1.00': '0.0613',
  'Magnetogyric_ratio (γ)': '1.1282403',
  'Magnetic_moment μ (μN)': '0.20400919',
  'Nuclear_quad_mom (Q/millibarn)': '',
  'Line_width_factor 1056 l (m4)': '-'})

nmr_mod['uranium']['Isotope_233U'].update({'Natural abundance %': '',
  'Spin (I)': '',
  'Frequency relative to 1H = 100 (MHz)': '',
  'Receptivity_DP_relative_to_1H = 1.00': '',
  'Receptivity_DC_relative_to_13C = 1.00': '',
  'Magnetogyric_ratio (γ)': '',
  'Magnetic_moment μ (μN)': '',
  'Nuclear_quad_mom (Q/millibarn)': '3663(8)',
  'Line_width_factor 1056 l (m4)': ''})

nmr_mod['uranium']['Isotope_235U'].update({'Natural abundance %': '0.7200',
  'Spin (I)': '7/2',
  'Frequency relative to 1H = 100 (MHz)': '1.841000',
  'Receptivity_DP_relative_to_1H = 1.00': '-',
  'Receptivity_DC_relative_to_13C = 1.00': '-',
  'Magnetogyric_ratio (γ)': '-0.52',
  'Magnetic_moment μ (μN)': '-0.43',
  'Nuclear_quad_mom (Q/millibarn)': '4936(6)',
  'Line_width_factor 1056 l (m4)': '-'})

# Lastly add the structured dict into df_iso dataframe

df_iso['nmr_mod'] = nmr_mod.values()



# Clean thermochemical data 
rem_keys= ['Units']
for i in df_therm['thermo_chem']:
     for key in i.copy(): # avoid dictionary changed size error during iteration using .copy method
        for k in rem_keys:
            if key==k:
                del i[k]
                
#dict keys whose values needs to be cleaned
mod_key=['Melting point', 'Boiling point', 'Liquid range', 'Critical temperature', 
      'Superconduction temperature', 'Enthalpy of fusion', 'Enthalpy of vaporisation', 'Enthalpy of atomisation']

for i in range(len(df_therm)):
    a=df_therm.loc[i,'thermo_chem']
    for k in mod_key:
        for a in df_therm.loc[i,'thermo_chem'].keys():
            if a==k and i<118:
                temp=df_therm.loc[i,'thermo_chem'][k]
                df_therm.loc[i,'thermo_chem'][k]=re.sub(r"(\(.*?\))|(-)|( kJ mol-1)|( K)|(about )|(maybe about)", '', temp)
                if df_therm.loc[i,'thermo_chem'][k]=="":
                    df_therm.loc[i,'thermo_chem'][k]=np.nan
                else:
                    df_therm.loc[i,'thermo_chem'][k]=float(df_therm.loc[i,'thermo_chem'][k])
                    

# Seperate and store thermdata in another column "atomic_thermdata" of df_therm dataframe.
df_therm['atomic_thermdata']=''
df_therm['atomic_thermdata'] = df_therm['thermo_chem'].copy(deep=True)
df_therm['atomic_thermdata'] = df_therm['thermo_chem'].apply(dict.copy)


rem_keys= ['Melting point', 'Boiling point', 'Liquid range', 'Critical temperature', 
      'Superconduction temperature', 'Enthalpy of fusion', 'Enthalpy of vaporisation', 'Enthalpy of atomisation']

for dictionary in df_therm['atomic_thermdata']:
    count=0
    for key in dictionary.copy(): # avoid dictionary changed size error during iteration using .copy method
        for k in rem_keys:
            if key==k:
                del dictionary[k]
    count=count+1

for i in range(len(df_therm)):
    for k,v in df_therm.loc[i,'atomic_thermdata'].items():
        for key,val in v.items():
            temp=val
            val=re.sub(r"(\*)|(no data)|(not known)", '', temp)
            v[key]=val
            if val=="":
                v[key]=np.nan

for i in range(len(df_therm)):
    for k,v in df_therm.loc[i,'atomic_thermdata'].items():
        for key in v.copy():
            if k==v[key]:
                del v[key]


# In[26]:


method_list


# In[27]:


count = 0
for obj in objs:
        obj.section_webele_metadata.source = 'Webelements'
        obj.section_webele_metadata.atomic_spin_setting = 'NA'
        obj.section_webele_metadata.atomic_method = 'NA'
        obj.section_webele_metadata.atomic_basis_set = 'NA'
        obj.density_of_solid = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Density_of_solid")])
        obj.youngs_mod = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Youngs_mod")])
        obj.rigidity_mod = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Rigidity_mod")])
        obj.bulk_mod = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Bulk_mod")])
        obj.poissons_ratio = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Poissons_ratio")])
        obj.mineral_hardness = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Mineral_hardness")])
        obj.brinell_hardness = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Brinell_hardness")])
        obj.vickers_hardness = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Vickers_hardness")])
        obj.electrical_resist = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Electrical_resistivity")])
        obj.thermal_conduct = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Thermal_conductivity")])
        obj.coeff_of_lte = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Coeff_of_lte")])
        obj.reflectivity = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Reflectivity")])
        obj.refract_index = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Refractive_index")])
        obj.vel_of_sound = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Velocity_of_sound")])
        obj.atomic_ebe = df_Webele.iloc[count,df_Webele.columns.get_loc("Electron_bin_En")]
        obj.atomic_enc = df_Webele.iloc[count,df_Webele.columns.get_loc("Eff_Nuc_Char")]
        obj.atomic_ie = df_Webele.iloc[count,df_Webele.columns.get_loc("Ionisation_En")]
        obj.atomic_ea = float(df_Webele.iloc[count,df_Webele.columns.get_loc("Electron_aff")])
        try:
            obj.atomic_radius_calculated = float(df_atomic_sizes.iloc[count,df_atomic_sizes.columns.
                                                                  get_loc("atom_radii")]
                                                 ['Atomic radius (calculated)'])
        except ValueError:
            obj.atomic_radius_calculated = np.nan
                                                 
        try:
            obj.atomic_radius = float(df_atomic_sizes.iloc[count,df_atomic_sizes.columns.get_loc("atom_radii")]
                                 ['Atomic radius (empirical)'])
        except ValueError:
            obj.atomic_radius = np.nan
                                                 
        try:
            obj.covalent_rad = float(df_atomic_sizes.iloc[count,df_atomic_sizes.columns.get_loc("atom_radii")]
                                 ['Covalent radius (2008 values)'])
        except ValueError:
            obj.covalent_rad = np.nan
        
        try:
            obj.covalent_rad_emp = float(df_atomic_sizes.iloc[count,df_atomic_sizes.columns.get_loc("atom_radii")]
                                 ['Covalent radius (empirical)'])
        except ValueError:
            obj.covalent_rad_emp = np.nan
                                                 
        try:
            obj.van_der_waals_rad =float(df_atomic_sizes.iloc[count,df_atomic_sizes.columns.get_loc("atom_radii")]
                                 ['van der Waals radius'])
        except ValueError:
            obj.van_der_waals_rad = np.nan

        obj.covalent_radius_mol = df_as_copy.iloc[count,df_as_copy.columns.get_loc("atom_radii")]
        obj.atomic_orbital_radii =  df_atomic_sizes.iloc[count,df_atomic_sizes.columns.get_loc("orbital_radii")]
        obj.atomic_ionic_radii =  df_atomic_sizes.iloc[count,df_atomic_sizes.columns.get_loc("ionic_radii")]
        obj.paul_ionic_radii =  df_atomic_sizes.iloc[count,df_atomic_sizes.columns.get_loc("paul_ionic_radii")]
        
        obj.atomic_spacegroup = df_crys_struct.iloc[count,df_crys_struct.columns.get_loc("cryst_struct")]['Space group']
        obj.atomic_spacegroupnum= float(df_crys_struct.iloc[count,df_crys_struct.columns.get_loc("cryst_struct")]
                                       ['Space group number'])
        obj.atomic_structure = df_crys_struct.iloc[count,df_crys_struct.columns.get_loc("cryst_struct")]['Structure']
        obj.atomic_cellparam = df_crys_struct.iloc[count,df_crys_struct.columns.get_loc("atomic_cellparam")]
        
        obj.atomic_en =df_en.iloc[count,df_en.columns.get_loc("electronegativity")]
        obj.atomic_en_allen = float(df_en.iloc[count,df_en.columns.get_loc("electronegativity")]
        ['Allen electronegativity'])
        obj.atomic_en_paul = float(df_en.iloc[count,df_en.columns.get_loc("electronegativity")]
        ['Pauling electronegativity'])
        obj.atomic_en_allredroch = float(df_en.iloc[count,df_en.columns.get_loc("electronegativity")]
        ['Allred Rochow electronegativity'])
        obj.atomic_en_sanderson = float(df_en.iloc[count,df_en.columns.get_loc("electronegativity")]
        ['Sanderson electronegativity'])
        
        obj.atomic_nat_iso = df_iso.iloc[count,df_iso.columns.get_loc("nat_isotopes")]
        obj.atomic_rad_iso = df_iso.iloc[count,df_iso.columns.get_loc("radiosotope_data")]
        obj.atomic_nmr = df_iso.iloc[count,df_iso.columns.get_loc("nmr_mod")]
        
        obj.boiling_point = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
        ['Boiling point'])
        obj.supercond_temp = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
                                   ['Superconduction temperature'])
        obj.melting_point = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
        ['Melting point'])
        obj.liquid_range = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
        ['Liquid range'])
        obj.critical_temperature = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
        ['Critical temperature'])
        obj.atomic_hfu = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
        ['Enthalpy of fusion'])
        obj.atomic_hvap = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
        ['Enthalpy of vaporisation'])
        obj.atomic_hatm = float(df_therm.iloc[count,df_therm.columns.get_loc("thermo_chem")]
        ['Enthalpy of atomisation'])
        obj.atomic_thermdata = df_therm.iloc[count,df_therm.columns.get_loc("atomic_thermdata")]
        count+=1
        
atomic_element_symbol = df_Webele['Element_symbol'].values
for count, symbol in enumerate(atomic_element_symbol):
    globals()[symbol] = objs[count]


df_Webele.rename_axis('',inplace=True)
df_comb = df_Webele.copy(deep=True)
df_comb['atom_radii']= df_atomic_sizes['atom_radii'].values
df_comb['orbital_radii']= df_atomic_sizes['orbital_radii'].values
df_comb['ionic_radii']= df_atomic_sizes['ionic_radii'].values
df_comb['covalent_radius_mol']= df_as_copy['atom_radii'].values
df_comb['paul_ionic_radii']= df_atomic_sizes['paul_ionic_radii'].values
df_comb['nat_isotopes']= df_iso['nat_isotopes'].values
df_comb['radiosotope_data']= df_iso['radiosotope_data'].values
df_comb['nmr_mod']= df_iso['nmr_mod'].values
df_comb['electronegativity']= df_en['electronegativity'].values
df_comb['cryst_struct']= df_crys_struct['cryst_struct'].values
df_comb['atomic_cellparam']= df_crys_struct['atomic_cellparam'].values
df_comb['thermo_chem']= df_therm['thermo_chem'].values
df_comb['atomic_thermdata']= df_therm['atomic_thermdata'].values



def definition(abc):
    '''
    This function can be used to access quantity definitions for each element features accessible
    '''
    if hasattr(atomic_properties_webele,abc):
        return print(getattr(atomic_properties_webele,abc).__doc__)
    elif hasattr(metadata,abc):
        return print(getattr(metadata,abc).__doc__)
    else:
        return print('Check the input feature name. Either it is wrong or requested feature does not exist from this webelements source') 


def symbol(abc):
    '''
This function utilty is, it can be called to access element properties based on its symbol in periodic table. Can be usefull if want to acess particular property of multiple elements at once
    '''
    return globals()[abc]





