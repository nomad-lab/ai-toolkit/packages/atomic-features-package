#!/usr/bin/env python
# coding: utf-8



from nomad.metainfo import MSection, Quantity, SubSection, units
import typing

"""metainfo module:

This module helps in defining the type of schema of data available from different material data sources like :- magpie,pymat etc. 
It uses the nomad python package for defining the schema. For additional info about this one can refer :-https://nomad-lab.eu/prod/rae/docs/metainfo.html
If any other source needs to be added in future, another class with appropriate name with all features should be defined as it is done here eg. for pymatgen."""



class metadata(MSection):
    '''
    This section contains metadata information of source,method and functional used for atomic features.
    
    '''
    source = Quantity(type = str, description='''Provides the name of source 
    of particular atomic feature''')
    atomic_method = Quantity(type = str, description='''Provides the name of functional used for calculation 
    of particular atomic feature''')
    atomic_basis_set= Quantity(type = str, description='''Provides the unique name of basis set applied for computation''')
    atomic_spin_setting = Quantity(type = str, description='''Provides information whether spin settings are turned on or off''')


class atomic_properties_fhi(MSection):
    section_fhi_metadata = SubSection(sub_section=metadata)
    '''
    This section contains metadata information of several atomic features accessible from FHI as source
    https://gitlab.mpcdf.mpg.de/nomad-lab/atomic_features_fhi_aims_really_tight/-/tree/master/csv 
    
    Note : If spin settings are set to "TRUE", then first element of of any atomic property for eg: atomic_r_s[0] corresponds to spin up configuration and atomic_r_s[1] corresponds to spin down configuration. This is consistent for all the atomic properties defined in this class.  
    '''
    atomic_number = Quantity(type = int, description='''Provides the number of protons found in nucleus''')
    atomic_element_symbol = Quantity(type = str, description='''Provides the symbol of element as per periodic table''')
    atomic_r_s = Quantity(type = float, description='''Provides the s orbital atomic radii''',shape=['0..*'],unit = units.angstrom)
    atomic_r_p = Quantity(type = float, description='''Provides the p orbital atomic radii''',shape=['0..*'],unit = units.angstrom)
    atomic_r_d = Quantity(type = float, description='''Provides the d orbital atomic radii''',shape=['0..*'],unit = units.angstrom)
    atomic_r_val = Quantity(type = float, description='''Provides the atomic radii of element''',shape=['0..*'],unit = units.angstrom)
    atomic_ea = Quantity(type = float, description='''Provides the atomic electron affinity calculated 
    from energy difference''',unit = units.joule)
    atomic_ip = Quantity(type = float, description='''Provides the atomic ionization potential calculated  from energy difference''',unit = units.joule)
    atomic_hfomo = Quantity(type = float, description='''Provides the energy of highest of fully filled molecular orbital''',unit = units.joule)
    atomic_hpomo = Quantity(type = float, description='''Provides the energy of highest partially filled molecular orbital''',unit = units.joule)
    atomic_lfumo = Quantity(type = float, description='''Provides the energy of lowest fully filled molecular orbital''',unit = units.joule)
    atomic_lpumo = Quantity(type = float, description='''Provides the energy lowest partially filled molecular orbital''',unit = units.joule)
    atomic_ea_by_half_charged_homo = Quantity(type = float, description='''Provides the half charged atomic electron affinity 
    of HOMO''',unit = units.joule)
    atomic_ip_by_half_charged_homo = Quantity(type = float, description='''Provides the half charged atomic ionization potential 
    of HOMO''',unit = units.joule)
    atomic_r_s_neg_1 = Quantity(type = float, description='''Provides the s orbital atomic radii of -1 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_p_neg_1 = Quantity(type = float, description='''Provides the p orbital atomic radii of -1 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_d_neg_1 = Quantity(type = float, description='''Provides the d orbital atomic radii of -1 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_val_neg_1 = Quantity(type = float, description='''Provides the atomic radii of -1 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_s_neg_05 = Quantity(type = float, description='''Provides the s orbital atomic radii of -0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_p_neg_05 = Quantity(type = float, description='''Provides the p orbital atomic radii of -0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_d_neg_05 = Quantity(type = float, description='''Provides the d orbital atomic radii of -0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_val_neg_05 = Quantity(type = float, description='''Provides the atomic radii of -0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_s_05 = Quantity(type = float, description='''Provides the s orbital atomic radii of +0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_p_05 = Quantity(type = float, description='''Provides the p orbital atomic radii of +0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_d_05 = Quantity(type = float, description='''Provides the d orbital atomic radii of +0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_val_05 = Quantity(type = float, description='''Provides the atomic radii of +0.5 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_s_1 = Quantity(type = float, description='''Provides the s orbital atomic radii of +1 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_p_1 = Quantity(type = float, description='''Provides the p orbital atomic radii of +1 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_d_1 = Quantity(type = float, description='''Provides the d orbital atomic radii of +1 charged''',shape=['0..*'],unit = units.angstrom)
    atomic_r_val_1 = Quantity(type = float, description='''Provides the atomic radii of +1 charged''',shape=['0..*'],unit = units.angstrom)



class atomic_properties_pymat(MSection):
    section_pymat_metadata = SubSection(sub_section=metadata)
    '''
    This section contains metadata schema definition of several atomic features accessible from pymatgen as source 
    https://github.com/materialsproject/pymatgen/blob/master/pymatgen/core/periodic_table.json 
    '''
    atomic_number = Quantity(type = int, description='''Provides the number of protons found in nucleus''')
    atomic_element_symbol = Quantity(type = str, description='''Provides the symbol of element as per periodic table''')
    atomic_mass = Quantity(type = float, description='''Provides atomic mass for the element''')
    atomic_radius = Quantity(type = float, description='''Provides atomic radius for the element''',unit = units.angstrom)
    atomic_radius_calculated = Quantity(type = float, description='''Provides Calculated atomic radius for the element ''',unit = units.angstrom)
    atomic_orbitals = Quantity(type = typing.Any,description='''Provides energy of the atomic orbitals as a dict''',unit = units.eV)
    boiling_point = Quantity(type = float, description='''Provides Boiling point to selected element''',unit = units.K)
    brinell_hardness = Quantity(type = float, description='''Provides Brinell Hardness of selected element''',unit=units.MN/(units.meter*units.meter))
    bulk_modulus = Quantity(type = typing.Any, description='''Provides Bulk modulus of selected element : GPa Unit''',unit = units.GPa)
    coeff_olte = Quantity(type = float, description='''Provides Coefficient of linear thermal expansion (10e-6)''')
    common_ox_states = Quantity(type = typing.Any, description='''Provides the list of common oxidation states in which the 
    element is found''')
    critical_temperature = Quantity(type = float, description='''Provides the Critical temperature of element''',unit = units.K)
    density_of_solid = Quantity(type = float, description='''Provides the density of solid phase''',unit = units.kg/(units.metre*units.metre*units.metre))
    electrical_resistivity = Quantity(type = typing.Any, description='''Provides the electrical resistivity of element(x10e-8)''',unit = (units.ohm*units.meter))
    electronic_structure = Quantity(type = typing.Any, description='''Provides the Electronic structure as string, with only valence electrons''')
    ionic_radii = Quantity(type = typing.Any, description='''Provides All ionic radii of the element as a dict of {oxidation state: ionic radii}''',unit = units.angstrom)
    liquid_range = Quantity(type = float, description='''Provides the liquid range temp. of the element ''',unit = units.K)
    melting_point = Quantity(type = typing.Any, description='''Provides the Melting point of the element''',unit = units.K)
    mendeleev_no = Quantity(type = float, description='''Provides the Mendeleev number from definition given by 
    Pettifor, D. G. (1984). A chemical scale for crystal-structure maps. Solid State Communications, 51 (1), 31-34''')
    mineral_hardness = Quantity(type = typing.Any, description='''Provides the Mineral Hardness of the element ''',unit = units.dimensionless)
    molar_volume = Quantity(type = float, description='''Provides the Molar volume of the element''',unit = units.cubic_centimeter)
    atomic_element_name = Quantity(type = str, description='''Provides name of the full long name of the element''')
    oxidation_states = Quantity(type = typing.Any, description='''Provides the python list of all known oxidation states''')
    poisson_ratio = Quantity(type = float, description='''Provides the Poisson's ratio of the element''',unit = units.dimensionless)
    atomic_refelctivity = Quantity(type = float, description='''Provides the Reflectivity of the element : % Unit''')
    atomic_refractive_index = Quantity(type = typing.Any, description='''Provides the Refractice index of the element''')
    rigidity_modulus = Quantity(type = float, description='''Provides the  Rigidity modulus of the element''',unit = units.GPa)
    shannon_radii = Quantity(type = typing.Any, description='''Provides the (python) dictionary (key-value pairs) Shannon radius for specie in the different environments
    Oxdiation_no : -4...6
    cn: Coordination using roman letters. Values are I-IX, as well as IIIPY, IVPY and IVSQ.
    spin: Some species have different radii for different spins. One can get specific values using "High Spin" or"Low Spin". 
    If no spin data is available a blank-space is used "". If only one spin data is available, it is returned and this spin parameter is ignored.
    radius_type: Either "crystal" or "ionic"
    typical dict would have key in specific format "Oxdiation_no.cn.spin.crystal_radius" ''')

    supercond_temp = Quantity(type = typing.Any, description='''Provides the  Superconduction temperature of the element''',unit = units.K)
    thermal_cond = Quantity(type = typing.Any, description='''Provides the Thermal conductivity of the element ''',unit = units.W/units.mK)
    van_der_waals_rad = Quantity(type = float, description='''Provides the Van der Waals radius for the element. This is the 
    empirical value(http://en.wikipedia.org/wiki/Atomic_radii_of_the_elements_(data_page))''',unit = units.angstrom)
    velocity_of_sound = Quantity(type = float, description='''Provides the Velocity of sound in the element''',unit = units.m/units.s)
    vickers_hardness = Quantity(type = float, description='''Provides the Vicker's hardness of the element''',unit = units.MN/(units.m*units.m))
    x = Quantity(type = float, description='''Provides the Pauling electronegativity. Elements without an electronegativity
    number are assigned a value of zero by default''')
    youngs_modulus = Quantity(type = float, description='''Provides the Young's modulus of the element''',unit = units.GPa)
    metallic_radius = Quantity(type = float, description='''Provides the Metallic radius of the element''',unit = units.angstrom)
    iupac_ordering = Quantity(type = float, description='''Ordering according to Table VI of "Nomenclature of Inorganic Chemistry
    (IUPAC Recommendations 2005)". This ordering effectively follows the groups and rows of the periodic table, except the 
    Lanthanides, Actanides and hydrogen''')
    icsd_oxd_states = Quantity(type = typing.Any, description='''Provides list(python) of all oxidation states with 
    at least 10 instances in ICSD database AND at least 1% of entries for that element''')
    nmr_quadrapole_mom = Quantity(type = typing.Any, description='''Provides a dictionary of the nuclear electric 
    quadrupole moment in units of e*millibarns for various isotopes''')
    max_oxd_state = Quantity(type = float, description='''Provides the Maximum oxidation state for element''')
    min_oxd_state = Quantity(type = float, description='''Provides the Minimum oxidation state for element''')
    ionic_radii_hs = Quantity(type = typing.Any, description='''Provides the Ionic radius of specie''',unit = units.angstrom)
    ionic_radii_ls = Quantity(type = typing.Any, description='''Provides the Ionic radius of specie''',unit = units.angstrom)

class atomic_properties_lda2015(MSection):
    section_lda2015_metadata = SubSection(sub_section=metadata)
    '''
    This section contains metadata schema definition of several atomic features accessible from lda2015 paper as source
      
    '''
    
    atomic_number = Quantity(type = int, description='''Provides the number of protons found in nucleus''')
    atomic_element_symbol = Quantity(type = str, description='''Provides the symbol of element as per periodic table''')
    atomic_r_s = Quantity(type = float, description='''Provides the s orbital atomic radii''',unit = units.angstrom)
    atomic_r_p = Quantity(type = float, description='''Provides the p orbital atomic radii''',unit = units.angstrom)
    atomic_r_d = Quantity(type = float, description='''Provides the d orbital atomic radii''',unit = units.angstrom)
    atomic_period = Quantity(type = int, description='''Provides the period number to which the element belongs in periodic table''',unit = units.dimensionless)
    atomic_ea = Quantity(type = float, description='''Provides the atomic electron affinity calculated 
    from energy difference''',unit = units.eV)
    atomic_ip = Quantity(type = float, description='''Provides the atomic ionization potential calculated  from energy difference''',unit = units.eV)
    atomic_homo = Quantity(type = float, description='''Provides the energy of highest fully filled molecular orbital''',unit = units.angstrom)
    atomic_lumo = Quantity(type = float, description='''Provides the energy of lowest fully filled molecular orbital''',unit = units.angstrom)

class atomic_properties_matminer(MSection):
    section_matmin_metadata = SubSection(sub_section=metadata)
    '''
    This section contains metadata schema definition of several atomic features accessible from Magpie as source
    https://github.com/hackingmaterials/matminer/tree/master/matminer/utils/data_files/magpie_elementdata  
    '''
    
    atomic_number = Quantity(type = int, description='''Atomic number''')
    atomic_element_symbol = Quantity(type = str, description='''Provides the symbol of element as per periodic table''')
    atomic_radius = Quantity(type = float, description='''Atomic radius''',unit = units.angstrom)
    atomic_volume = Quantity(type = float, description='''Volume of an atom of each element : A^3 / atom''')
    atomic_weight = Quantity(type = float, description='''Atomic weight''')
    bulk_modulus = Quantity(type = float, description='''Bulk modulus''',unit = units.GPa)
    boiling_temp = Quantity(type = float, description='''Boiling temperature''',unit = units.K)
    periodict_column = Quantity(type = int, description='''Column on periodic table 
                                Notes: Column of all RE elements is listed as 3''')
    covalent_radius = Quantity(type = float, description='''Covalent radius of each element ''',unit = units.pm)
    atomic_density = Quantity(type = float, description='''Density of element at STP : g/L ''',unit = units.g/units.L)
    atomic_ea = Quantity(type = float, description='''Electron affinitiy''',unit = units.kJ/units.mol)
    atomic_en = Quantity(type = float, description='''Pauling electronegativity ''',unit = units.dimensionless)
    atomic_ie_1 = Quantity(type = float, description='''Energy to remove the first electron from an element''',unit = units.eV)
    gs_bandgap = Quantity(type = float, description='''DFT bandgap energy of T=0K ground state''',unit = units.eV)
    gs_bcclatparam = Quantity(type = float, description='''Estimated BCC lattice parameter based on the DFT volume of the 
    OQMD ground state for each element''')
    atomic_cpmass = Quantity(type = float, description='''Specific heat capacity at STP''',unit = units.J/(units.g*units.K))
    atomic_cpmolar = Quantity(type = float, description='''Molar heat capacity at STP''',unit = units.J/(units.mol*units.K))
    atomic_hfu = Quantity(type = float, description='''Enthalpy of fusion for elements at their melting temperatures''',unit = units.kJ/units.mol)
    atomic_hvap = Quantity(type = float, description='''Enthalpy of vaporization for elements at their boiling temperatures''',unit = units.kJ/units.mol)
    gs_fcclatparam = Quantity(type = float, description='''Estimated FCC lattice parameter based on the DFT volume of the 
    OQMD ground state for each element ''',unit = units.angstrom)
    gs_energy_pa = Quantity(type = float, description='''DFT energy per atom (raw VASP value) of T=0K ground state : eV/atom ''')
    gs_mag_mom = Quantity(type = float, description='''DFT magnetic momenet of T=0K ground state''')
    gs_volume_pa = Quantity(type = float, description='''DFT volume per atom of T=0K ground state : Angs^3/atom''')
    atomic_hhip = Quantity(type = float, description='''Herfindahl-Hirschman Index (HHI) production values''')
    atomic_hhir = Quantity(type = float, description='''Herfindahl-Hirschman Index (HHI) reserves values''')
    atomic_ICSD_vol = Quantity(type = float, description='''Volume per atom of ICSD phae at STP''')
    melting_point = Quantity(type = float, description='''Melting temperature of element''',unit = units.K)
    mendeleev_no = Quantity(type = float, description='''Mendeleev Number(position on the periodic table, counting columnwise 
    from H)''')
    micracle_radius = Quantity(type = float, description='''Assessed radii of elements in metallic glass structures''',unit = units.pm)
    molar_volume = Quantity(type = float, description='''Molar Volume''',unit = units.cubic_centimeter)
    atomic_ws3 = Quantity(type = float, description='''Electron density at surface of Wigner-Sietz cell. Used in Miedema's model''')
    atomic_ndunf = Quantity(type = float, description='''Property: Number of unfilled d valence orbitals
                                    Notes: Number of unoccupied orbitals = 0 if shell unoccupied, Maximum-Filled if occupied''')
    atomic_ndval = Quantity(type = float, description='''Number of filled d valence orbitals''',unit = units.dimensionless)
    atomic_nfunf = Quantity(type = float, description='''Property: Number of unfilled f valence orbitals
                                    Notes: Number of unoccupied orbitals = 0 if shell unoccupied, Maximum-Filled if occupied''',unit = units.dimensionless)
    atomic_nfval = Quantity(type = float, description='''Number of filled f valence orbitals''',unit = units.dimensionless)
    atomic_npunf = Quantity(type = float, description='''Number of unfilled p valence orbitals
                                    Notes: Number of unoccupied orbitals = 0 if shell unoccupied, Maximum-Filled if occupied''',unit = units.dimensionless)
    atomic_npval = Quantity(type = float, description='''Number of filled p valence orbitals''',unit = units.dimensionless)
    atomic_nsunf = Quantity(type = float, description='''Number of unfilled s valence orbitals
                                    Notes: Number of unoccupied orbitals = 0 if shell unoccupied, Maximum-Filled if occupied''',unit = units.dimensionless)
    atomic_nsval = Quantity(type = float, description='''Number of filled s valence orbitals''',unit = units.dimensionless)
    atomic_nunf = Quantity(type = float, description='''Number of unfilled valence orbitals 
                                    Notes: Number of unoccupied orbitals = 0 if shell unoccupied, Maximum-Filled if occupied''',unit = units.dimensionless)
    atomic_nval = Quantity(type = float, description='''Number of valence electrons''',unit = units.dimensionless)
    atomic_oxstates = Quantity(type = typing.Any, description='''Observed oxidation states for each element''',unit = units.dimensionless)
    atomic_pol = Quantity(type = float, description='''Static average electric dipole polarizability : 10^-24''')
    atomic_phi = Quantity(type = float, description='''Adjusted work function (used in the Miedema's model)''',unit = units.eV)
    periodict_row = Quantity(type = int, description='''Row on periodic table''',unit = units.dimensionless)
    atomic_ie_2 = Quantity(type = float, description='''Energy to remove the second electron from an element''',unit = units.eV)
    shear_mod = Quantity(type = float, description='''Shear modulus''',unit = units.GPa)
    atomic_sgn = Quantity(type = float, description='''Space group of T=0K ground state structure 
        Source: Only structures from the ICSD were evaluated  Pm used OQMD groundstate in Spacegroup 194,
        which is only 4 meV/atom below 166. No ICSD structure''')
    thermal_cond = Quantity(type = float, description='''Thermal conductivity''',unit = units.W/units.mK)
    atomic_vdw_radius = Quantity(type = float, description='''Van der Waals radius''',unit = units.angstrom)
    atomic_pp_r_s = Quantity(type = float, description='''Psuedopotential radius of s orbital : a.u ''')
    atomic_pp_r_p = Quantity(type = float, description='''Psuedopotential radius of p orbital : a.u ''')
    atomic_pp_r_d = Quantity(type = float, description='''Psuedopotential radius of d orbital : a.u ''')
    atomic_pp_r_sig = Quantity(type = float, description='''Sum of the radii of s and p orbitals : a.u ''')
    atomic_pp_r_pi = Quantity(type = float, description='''Absolute value of the different between the radii of s and 
    p orbitals : a.u ''')
    atomic_isalkali = Quantity(type = float, description='''Boolean value of whether an element is an alkali or alkali earth metal''',unit = units.dimensionless)
    atomic_isdblock = Quantity(type = float, description='''Boolean value of whether an element is a d-block metal''',unit = units.dimensionless)
    atomic_isfblock = Quantity(type = float, description='''Boolean value of whether an element is an f-block metal''',unit = units.dimensionless)
    atomic_ismetal = Quantity(type = float, description='''Boolean value of whether an element is a metal''',unit = units.dimensionless)
    atomic_ismetalloid = Quantity(type = float, description='''Boolean value of whether an element is a metalloid''',unit = units.dimensionless)
    atomic_isnonmetal = Quantity(type = float, description='''Boolean value of whether an element is a nonmetal''',unit = units.dimensionless)
    atomic_en_allen = Quantity(type = float, description='''Allen electronegativity ''',unit = units.dimensionless)
    atomic_ie = Quantity(type = typing.Any, description='''Energy to remove the 1st,2nd,3rd..  electron from an element''',unit= units.eV)
    thermal_cond_log = Quantity(type = float, description='''Logarithmic Thermal conductivity ''')
    
class atomic_properties_webele(MSection):
    section_webele_metadata = SubSection(sub_section=metadata)
    '''
    This section contains metadata information of several atomic features accessible from Webelements as source  
    '''
    
    atomic_element_symbol = Quantity(type = str, description='''Provides the symbol of element as per periodic 
    table''')
    density_of_solid = Quantity(type = float, description='''Density refers to the mass contained within a unit 
    volume under specified conditions. Data given here refer to the solid ''',
                                unit = units.kg/(units.metre*units.metre*units.metre))
    molar_vol = Quantity(type = float, description='''The molar volume is equal to the atomic weight divided 
    by the density''',unit = units.cubic_centimeter)
    youngs_mod = Quantity(type = float, description='''Young's modulus refers to longitudinal 
    strain''',unit = units.GPa)
    rigidity_mod = Quantity(type = float, description='''The rigidity modulus refers to the change of shape produced 
    by a tangential stress''',unit = units.GPa)
    bulk_mod = Quantity(type = float, description='''The bulk modulus refers to the ratio of pressure to the decrease in volume. 
    This is the inverse of compressibility''',unit = units.GPa)
    poissons_ratio = Quantity(type = float, description='''Poisson's ratio is the lateral contraction per unit breadth divided by the 
    longitudinal extension per unit length''',unit = units.dimensionless)
    mineral_hardness = Quantity(type = float, description='''Hardness ranges on scale from 1 to 10 and mineralogical hardness depends upon the 
    ability of a material to scratch another''',unit = units.dimensionless)
    brinell_hardness = Quantity(type = float, description='''Provides Brinell Hardness of the element''',
                                unit = units.MN/(units.m*units.m))
    vickers_hardness = Quantity(type = float, description='''Provides the Vicker's hardness of the element''',
                                unit = units.MN/(units.m*units.m))
    electrical_resist = Quantity(type = float, description='''The electrical resistivity, or specific resistance, ρ, 
    is the resistance between the opposite faces of a metre cube of a material(10e-8)''',
                                 unit = (units.ohm*units.meter))
    thermal_conduct = Quantity(type = float, description='''The thermal conductivity, λ, is the quantity of heat 
    transmitted, due to unit temperature gradient, in unit time under steady conditions in a direction normal to 
    a surface of unit area, when the heat transfer is dependent only on the temperature gradient''',
                               unit = units.W/units.mK)
    coeff_of_lte = Quantity(type = float, description='''The coefficient of linear thermal expansion is the ratio of
    the change in length per degree K to the length at 273 K(10e6)''',
                            unit = units.dimensionless/units.K)
    reflectivity = Quantity(type = float, description='''The reflectivity is the percentage light reflected from a 
    surface at a given wavelength: % Unit''')
    refract_index = Quantity(type = float, description='''The refractive index is quoted for the gas or vapour state
    at 273 K/760 mm Hg for the sodium yellow d-line at 589.3 nm''',unit = units.dimensionless)
    vel_of_sound = Quantity(type = float, description='''Provides the Velocity of sound in the element''',
                            unit = units.m/units.s)
    atomic_ea = Quantity(type = float, description='''Electron affinitiy ''',unit = units.kJ/units.mol)
    atomic_ebe = Quantity(type = typing.Any, description='''Returns electron binding energies for atom as a dict''',
                          unit = units.eV)
    atomic_ie = Quantity(type = typing.Any, description='''Returns ionization energies of the atoms as a dict, 
    Units : Enthalpy / kJ mol‑1''')
    atomic_enc = Quantity(type = typing.Any, description='''Returns "Clementi-Raimondi" effective nuclear charges of 
    atoms as dict, Zeff ''')
    atomic_radius = Quantity(type = float, description='''Provides atomic radius for the element''',unit = units.pm)
    atomic_radius_calculated = Quantity(type = float, description='''Provides Calculated atomic radius for the 
    element ''',unit = units.pm)
    covalent_rad = Quantity(type = float, description='''Covalent radius of atom (2008)''',unit = units.pm)
    covalent_rad_emp = Quantity(type = float, description='''Covalent radius of atom''',unit = units.pm)
    covalent_radius_mol = Quantity(type = typing.Any, description='''Returns single/double/triple bond covalent radii using specific group 
    valencies and oxidation numbers of element as dict''',unit = units.pm)
    van_der_waals_rad = Quantity(type = float, description='''Provides the Van der Waals radius for the element'''
                                 ,unit = units.pm)
    atomic_orbital_radii = Quantity(type = typing.Any, description='''Returns calculated values of valence shell 
    orbital radii (Rmax) as dict, Units : Atomic unit lengths''')
    atomic_ionic_radii = Quantity(type = typing.Any, description='''Returns Shannon ionic radii with ion as key 
    and cordination number,radii in pm as dict''',unit = units.pm)
    paul_ionic_radii = Quantity(type = typing.Any, description='''Returns pauling ionic radii with ion as key 
    and pauling ionic radii in pm as dict''',unit = units.pm)
    atomic_en = Quantity(type = typing.Any, description='''All values are quoted on the Pauling scale.
    Returns types of electronegativity with its values as a dict.''')
    atomic_en_allen = Quantity(type = float, description='''All values are quoted on the Pauling scale.
    Returns Allen electronegativity ''')
    atomic_en_paul = Quantity(type = float, description='''Values are quoted on the Pauling scale.
    Returns Pauling electronegativity ''')
    atomic_en_allredroch = Quantity(type = float, description='''Values are quoted on the Pauling scale.
    Returns Allred Rochow electronegativity ''')
    atomic_en_sanderson = Quantity(type = float, description='''Values are quoted on the Pauling scale.
    Returns Sanderson electronegativity ''')
    atomic_nat_iso = Quantity(type = typing.Any, description='''Returns naturally occurring isotopes data as dict''')
    atomic_rad_iso = Quantity(type = typing.Any, description='''Returns radio isotope data as dict''')
    atomic_nmr = Quantity(type = typing.Any, description='''Returns NMR properties data as dict''')
    atomic_spacegroup = Quantity(type = str, description='''Returns Space group of elemental solid''')
    atomic_spacegroupnum = Quantity(type = float, description='''Returns Space group number of elemental solid''')
    atomic_structure = Quantity(type = str, description='''Returns structure type of elemental solid''')
    atomic_cellparam = Quantity(type = typing.Any, description='''Returns dict of cell parameters of 
    elemental solid''')
    supercond_temp = Quantity(type = typing.Any, description='''Provides the  Superconduction temperature of 
    the element''',unit = units.K)
    critical_temperature = Quantity(type = float, description='''Provides the Critical temperature of 
    element''',unit = units.K)
    liquid_range = Quantity(type = float, description='''Provides the liquid range temp. 
    of the element ''',unit = units.K)
    boiling_point = Quantity(type = float, description='''Provides Boiling point to 
    selected element''',unit = units.K)
    melting_point = Quantity(type = typing.Any, description='''Provides the Melting point of 
    the element''',unit = units.K)
    atomic_hfu = Quantity(type = float, description='''Enthalpy of fusion for elements at their 
    melting temperatures''',unit = units.kJ/units.mol)
    atomic_hvap = Quantity(type = float, description='''Enthalpy of vaporization for elements at 
    their boiling temperatures''',unit = units.kJ/units.mol)
    atomic_hatm = Quantity(type = float, description='''The enthalpy change that accompanies the total 
    separation of all atoms in a chemical substance''',unit = units.kJ/units.mol)
    atomic_thermdata = Quantity(type = typing.Any, description='''Returns a dictionary of few thermodynamic data with keys as its elemental state.
    Units of the properties are as follows:
    ΔfH°: kJ mol‑1
    
    ΔfG°: kJ mol‑1
    
    S°: J K‑1 mol‑1
    
    CpH: J K‑1 mol‑1
    
    H°298.15‑H°0: kJ mol‑1 ''')
