import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="atomic-features-package", # Replace with your own username
    version="1.0.0",
    author="Aakash Ashok Naik",
    author_email="naik@fhi-berlin.mpg.de",
    description="Unified package to access atomic properites of elements",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.mpcdf.mpg.de/nomad-lab/atomic-features-package",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires= ['nomad-lab','numpy','pandas','mendeleev','bokeh','ipywidgets','typing','urllib3'],
    python_requires='>=3.6',
    include_package_data=True,
    package_data={
    'atomic-features-package': ['data/dft/*.csv'],
    'atomic-features-package': ['data/pymat/*.json'],
    'atomic-features-package': ['data/lda2015/*.csv'],
    'atomic-features-package': ['data/matminer/*.table'],
    'atomic-features-package': ['data/webele/*.csv'],
},
)
